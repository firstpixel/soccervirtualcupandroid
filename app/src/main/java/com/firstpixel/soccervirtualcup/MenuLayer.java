package com.firstpixel.soccervirtualcup;

import org.cocos2d.layers.CCColorLayer;
import org.cocos2d.layers.CCScene;
import org.cocos2d.menus.CCMenu;
import org.cocos2d.menus.CCMenuItem;
import org.cocos2d.menus.CCMenuItemLabel;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCLabel;
import org.cocos2d.nodes.CCSprite;
//import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGSize;
import org.cocos2d.types.ccColor3B;
import org.cocos2d.types.ccColor4B;

import android.view.MotionEvent;

public class MenuLayer extends CCColorLayer
{
	private static final String TAG = "Soccer-MenuLayer";
	protected CCLabel _label;
	protected CCMenuItem _menuItem0;
	protected CCMenuItem _menuItem1;
	protected CCMenuItem _menuItem2;
	protected CCMenuItem _menuItem3;
	protected CCMenu _menu;
	protected float scaleFactor = 1;
	
	public static CCScene scene()
	{
		CCScene scene = CCScene.node();
		CCColorLayer layer = new MenuLayer(ccColor4B.ccc4(255, 255, 255, 255));
		scene.addChild(layer);
		
		return scene;
	}
	
	protected MenuLayer(ccColor4B color)
	{
		super(color);
	//	this.setIsTouchEnabled(true);		
//		CGSize winSize = CCDirector.sharedDirector().displaySize();
		
		this.addBackGroundScene();

		CCLabel _labelPlayWorldCup = CCLabel.makeLabel("World Cup", "DroidSans", 20*scaleFactor);
		_labelPlayWorldCup.setColor(ccColor3B.ccWHITE);
		_menuItem0 = CCMenuItemLabel.item(_labelPlayWorldCup, this, "playWorldCup");

		CCLabel _labelPlaySingle = CCLabel.makeLabel("Single Match", "DroidSans", 20*scaleFactor);
		_labelPlaySingle.setColor(ccColor3B.ccWHITE);
		_menuItem1 = CCMenuItemLabel.item(_labelPlaySingle, this, "playSingleMatch");

		CCLabel _labelPlayVersus = CCLabel.makeLabel("Versus Mode", "DroidSans", 20*scaleFactor);
		_labelPlayVersus.setColor(ccColor3B.ccWHITE);
		_menuItem2 = CCMenuItemLabel.item(_labelPlayVersus, this, "playVersusMode");

		
		CCLabel _labelHelp = CCLabel.makeLabel("Help", "DroidSans", 20*scaleFactor);
		_labelHelp.setColor(ccColor3B.ccWHITE);
		_menuItem3 = CCMenuItemLabel.item(_labelHelp, this, "help");
		
		_menu = CCMenu.menu(_menuItem0,_menuItem1,_menuItem2,_menuItem3);
		_menu.alignItemsVertically(70);
		_menu.setPosition(205.0f*scaleFactor,120.0f*scaleFactor);
		
	    addChild(_menu);
		// Add menu to the scene
//	    CCTouchDispatcher.sharedDispatcher().addTargetedDelegate(this, 0, true);
	    this.setIsTouchEnabled(true);
	}

	private void addBackGroundScene() {
        CCSprite bgForLevel1 = CCSprite.sprite("screen/gamestart1.jpg");
        bgForLevel1 .setAnchorPoint(0, 0);	 
        // Get window size
    	CGSize s = CCDirector.sharedDirector().winSize();
    	// Get scale
    	float scaleX = s.width/bgForLevel1.getContentSize().width;
        float scaleY = s.height/bgForLevel1.getContentSize().height;
        // Set for minor scale
        if(scaleX<scaleY){
        	scaleFactor = scaleY;
        	bgForLevel1.setPosition(s.width-(bgForLevel1.getContentSize().width*scaleFactor) ,0.0f);
    	}else{
        	scaleFactor = scaleX;        
        	bgForLevel1.setPosition(0.0f,s.height-(bgForLevel1.getContentSize().height*scaleFactor) );
    	}
        bgForLevel1.setScale(scaleFactor);
		addChild(bgForLevel1 );
    }
	
	@Override
	public boolean ccTouchesBegan(MotionEvent event) {
	    return true;
	}
	
	
	@Override
	public boolean ccTouchesEnded(MotionEvent event) {
	    return super.ccTouchesEnded(event);
	}
	
	
	//GAME TYPE 0  worldcup
	//GAME TYPE 1 versus
	//GAME TYPE 2 single
	//GAME TYPE 3 Multiplayer
	//GAME TYPE 5 penalty - not here check PenaltyBallLayer 


	public void playWorldCup(Object sender) {
		GlobalSingleton.teste = "playWorldCup";
		GlobalSingleton.atualMatch = 0;
		GlobalSingleton.gameType = 0;
		CCDirector.sharedDirector().replaceScene(SettingsScene.scene());
	}
	public void playVersusMode(Object sender)
	{
		GlobalSingleton.teste = "playversus";
        GlobalSingleton.atualMatch = 0;
        GlobalSingleton.gameType = 1;
		CCDirector.sharedDirector().replaceScene(SettingsScene.scene());
	}
	public void playSingleMatch(Object sender)
	{
		GlobalSingleton.teste = "playsingle";
        GlobalSingleton.atualMatch = 0;
        GlobalSingleton.gameType = 2;
		CCDirector.sharedDirector().replaceScene(SettingsScene.scene());
	}

	public void help(Object sender)
	{
		GlobalSingleton.teste = "help";
		CCDirector.sharedDirector().replaceScene(AfterGameMatchsScene.scene());
	}
	
}
