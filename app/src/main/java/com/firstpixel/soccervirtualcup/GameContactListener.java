package com.firstpixel.soccervirtualcup;

import org.cocos2d.nodes.CCSprite;

import android.util.Log;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactListener;

public class GameContactListener implements ContactListener {

	private static final String TAG = "Soccer-GameContactListener";
	
	@Override
	public void beginContact(Contact contact) {
		// TODO Auto-generated method stub
		//Log.e("myContactListener"," contact point add"+contact.getFixtureA().getBody().getUserData());
		
	//tag==1000 ball
		if(contact.getFixtureA().getBody().getUserData()!=null && contact.getFixtureB().getBody().getUserData()!=null){
			//both are Sprites
			if(contact.getFixtureA().getBody().getUserData().getClass() == CCSprite.class  &&  contact.getFixtureB().getBody().getUserData().getClass() == CCSprite.class){
				CCSprite spriteA = (CCSprite)contact.getFixtureA().getBody().getUserData();
				CCSprite spriteB = (CCSprite)contact.getFixtureB().getBody().getUserData();
				
				if(spriteA.getTag()==1000 || spriteB.getTag()==1000 ){
					//Log.e("myContactListener", "Ball Hit");
				}
			// only A is Sprite
			}else if(contact.getFixtureA().getBody().getUserData().getClass() == CCSprite.class  &&  contact.getFixtureB().getBody().getUserData().getClass() != CCSprite.class){
				CCSprite spriteA = (CCSprite)contact.getFixtureA().getBody().getUserData();
				if(spriteA.getTag()==1000 && contact.getFixtureB().getBody().getUserData()=="goal1"){
					Log.e("myContactListener", "GOOOOL 1");
					GlobalSingleton.goal1Scored = true;
				}
				if(spriteA.getTag()==1000 && contact.getFixtureB().getBody().getUserData()=="goal2"){
					Log.e("myContactListener", "GOOOOL 2");
					GlobalSingleton.goal2Scored = true;
				}
				if(spriteA.getTag()==1000 && contact.getFixtureB().getBody().getUserData()=="corner1"){
					Log.e("myContactListener", "CORNER 1");
					GlobalSingleton.cornerImpulse = 1;
				}
				if(spriteA.getTag()==1000 && contact.getFixtureB().getBody().getUserData()=="wallTop"){
					Log.e("myContactListener", "WALLTOP ");
					GlobalSingleton.wallImpulse = 1;
				}
				
				
			//only B is Sprite	
			}else if(contact.getFixtureA().getBody().getUserData().getClass() != CCSprite.class  &&  contact.getFixtureB().getBody().getUserData().getClass() == CCSprite.class){
				CCSprite spriteB = (CCSprite)contact.getFixtureB().getBody().getUserData();
				if(spriteB.getTag()==1000 && contact.getFixtureA().getBody().getUserData()=="goal1"){
					Log.e("myContactListener", "GOOOOL 1");
					GlobalSingleton.goal1Scored = true;
				}
				if(spriteB.getTag()==1000 && contact.getFixtureA().getBody().getUserData()=="goal2"){
					Log.e("myContactListener", "GOOOOL 2");
					GlobalSingleton.goal2Scored = true;
				}
			}
		}
	}

	/*
	 // tag 7 == ball
	 if(sprite1.tag==7 ){
		if((sprite1.tag==7 && sprite2.tag==601) || (sprite1.tag==7 && sprite2.tag==602) || (sprite1.tag==7 && sprite2.tag==603) || (sprite1.tag==7 && sprite2.tag==604)){
			//NSLog(@"BALL HIT CORNER ADD IMPULSE!! %i",sprite2.tag);
			//NSLog(@"******************************************************************************");
			if(sprite2.tag==601)[ballLayer setCornerImpulse:1];
			if(sprite2.tag==602)[ballLayer setCornerImpulse:2];
			if(sprite2.tag==603)[ballLayer setCornerImpulse:3];
			if(sprite2.tag==604)[ballLayer setCornerImpulse:4];
			return 1;
		}
		if((sprite1.tag==7 && sprite2.tag==1001) || (sprite1.tag==7 && sprite2.tag==1002) || (sprite1.tag==7 && sprite2.tag==1003) 
		   || (sprite1.tag==7 && sprite2.tag==1004) || (sprite1.tag==7 && sprite2.tag==1005) || (sprite1.tag==7 && sprite2.tag==1006)){
			//NSLog(@"BALL HIT WALL ADD IMPULSE!! %i",sprite2.tag);
			//NSLog(@"******************************************************************************");
			if(sprite2.tag==1001)[ballLayer setWallImpulse:1];
			if(sprite2.tag==1002)[ballLayer setWallImpulse:2];
			if(sprite2.tag==1003)[ballLayer setWallImpulse:2];
			if(sprite2.tag==1004)[ballLayer setWallImpulse:3];
			if(sprite2.tag==1005)[ballLayer setWallImpulse:4];
			if(sprite2.tag==1006)[ballLayer setWallImpulse:4];
			return 1;
		}
		if((sprite1.tag==7 && sprite2.tag==101) || (sprite1.tag==7 && sprite2.tag==102) || (sprite1.tag==7 && sprite2.tag==103) || (sprite1.tag==7 && sprite2.tag==104)){
			if( [mySingleton  golAnimationPlaying]==NO  ){
				if((sprite1.tag==7 && sprite2.tag==103))[ballLayer golScored:2];
				if((sprite1.tag==7 && sprite2.tag==104))[ballLayer golScored:1];
			}
			return 0;
		}else{
			[ballLayer ballAudio];
			return 1;
		}
	}else{
		return 1;
	}
	 
	  */
	
	
	
	@Override
	public void endContact(Contact contact) {
		// TODO Auto-generated method stub
		
	}

}
