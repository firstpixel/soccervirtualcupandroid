package com.firstpixel.soccervirtualcup;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.google.android.gms.ads.*;

import org.cocos2d.layers.CCScene;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.opengl.CCGLSurfaceView;

import com.flurry.android.FlurryAgent;

public class SoccerVirtualCupActivity extends Activity
{
	static {
        System.loadLibrary("gdx");
	}
	
	protected CCGLSurfaceView _glSurfaceView;
	
	 /** The view to show the ad. */
	  private AdView adView;
	  /* Your ad unit id. Replace with your actual ad unit id. */
	  private static final String AD_UNIT_ID = "ca-app-pub-4523467891811991/4238110691";

	
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		GlobalSingleton.appContext = this;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

	}
	
	@Override
	public void onStart()
	{
		super.onStart();
		FlurryAgent.onStartSession(this, "4WXXNX7Q3Z6G2J5MQNJN");	
		
		_glSurfaceView = new CCGLSurfaceView(this);
		//setContentView(_glSurfaceView);
		
		CCDirector.sharedDirector().attachInView(_glSurfaceView);
		
		CCDirector.sharedDirector().setDeviceOrientation(CCDirector.kCCDeviceOrientationLandscapeLeft);
		
		CCDirector.sharedDirector().setDisplayFPS(true);
		
		CCDirector.sharedDirector().setAnimationInterval(1.0f / 60.0f);
		
		CCScene scene = MenuLayer.scene();
		
		setContentView(R.layout.main);
		
		FrameLayout layout = (FrameLayout)findViewById(R.id.layout_main);
        layout.addView(_glSurfaceView);

	       
		CCDirector.sharedDirector().runWithScene(scene);
		

		 // Create an ad.
	    adView = new AdView(this);
	    //adView.setAdSize(AdSize.BANNER);
	    adView.setAdSize(AdSize.FULL_BANNER);
	    adView.setAdUnitId(AD_UNIT_ID);

	    // Add the AdView to the view hierarchy. The view will have no size
	    // until the ad is loaded.
//		FrameLayout layout = (FrameLayout)findViewById(R.id.layout_main);
	    FrameLayout.LayoutParams adsParams =new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT, android.view.Gravity.BOTTOM|android.view.Gravity.CENTER_HORIZONTAL); 
	    layout.addView(adView, adsParams );
//	    layout.addView(adView);

	    // Create an ad request. Check logcat output for the hashed device ID to
	    // get test ads on a physical device.
	    
	    AdRequest adRequest = new AdRequest.Builder()
	        .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
	        .addTestDevice("464119643128452F")
	        .build();

	    // Start loading the ad in the background.
	    
	    adView.loadAd(adRequest);
	}
	
	@Override
	public void onPause()
	{
		super.onPause();
		CCDirector.sharedDirector().pause();
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		
		CCDirector.sharedDirector().resume();
	}
	
	@Override
	public void onStop()
	{
		//CCDirector.sharedDirector().end();
		super.onStop();
		FlurryAgent.onEndSession(this);
	//	
	}
	
    @Override
    public void onDestroy() {
        super.onDestroy();

      //  CCDirector.sharedDirector().end();
    }
}