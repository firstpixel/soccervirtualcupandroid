package com.firstpixel.soccervirtualcup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import org.cocos2d.actions.UpdateCallback;
import org.cocos2d.actions.CCScheduler;
import org.cocos2d.actions.interval.CCAnimate;
import org.cocos2d.actions.interval.CCScaleTo;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.config.ccMacros;
import org.cocos2d.layers.CCLayer; 
import org.cocos2d.layers.CCScene;
import org.cocos2d.nodes.CCAnimation;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCLabel;
import org.cocos2d.nodes.CCLabelAtlas;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.nodes.CCSpriteFrame;
import org.cocos2d.nodes.CCSpriteFrameCache;
import org.cocos2d.nodes.CCTextureCache;
import org.cocos2d.opengl.CCTexture2D;
//import org.cocos2d.nodes.CCSpriteSheet;
import org.cocos2d.sound.SoundEngine;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGRect;
import org.cocos2d.types.CGSize;
import org.cocos2d.types.ccColor3B;
import org.cocos2d.utils.Base64.InputStream;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.flurry.android.FlurryAgent;

import com.firstpixel.soccervirtualcup.AfterGameMatchsScene;

@SuppressWarnings("unused")
public class BallLayer extends CCLayer {

	    private static final String TAG = "Soccer-BallLayer";

	    protected static final float PTM_RATIO = 32.0f;
	    
	    protected static boolean hasSentPosition;
	    
	    protected static CCSprite itsYourTurn = null;
	    protected static CCSprite getReady = null;
	    protected static CCSprite waitPlaying = null;
	    
	    
		protected World _world;
	    protected static Body _body = null;
	    
	    protected static CCSprite player1Sprite = null;
	    protected static CCSprite player2Sprite = null;
		   
	    protected static CCSprite _ball = null;
	    protected static CCSprite _p1 = null;
	    protected static CCSprite _p1r = null;
	    protected static CCSprite _p2 = null;
	    protected static CCSprite _p2r = null;
	    protected static CCSprite _p3 = null;
	    protected static CCSprite _p3r = null;


	    protected static CCSprite _p4r = null;
	    protected static CCSprite _p4 = null;
	    protected static CCSprite _p5 = null;
	    protected static CCSprite _p5r = null;
	    protected static CCSprite _p6 = null;
	    protected static CCSprite _p6r = null;
	    protected static CCSprite touchSprite = null;
	    
	    
	    protected static CCSprite goal1 = null;
	    protected static CCSprite goal2 = null;
	    protected static CCSprite goal3 = null;
	    protected static CCSprite goal4 = null;
	    protected static CCSprite goal5 = null;
	    protected static CCSprite goal6 = null;
	    protected static CCSprite goal7 = null;
	    protected static CCSprite goal8 = null;
	    
	    protected static CCLabel labelPlayer1 = null;
	    protected static CCLabel labelPlayer2 = null;
	    protected static CCLabel labelTimerMinutes = null;
	    protected static CCLabel labelTimerSeconds = null;
	    protected static CCLabel p1Score = null;
	    protected static CCLabel p2Score = null;
	    
	    protected static CCSprite clockTurnSprite = null;
	    protected static CCSprite clockSprite = null;
	    protected static CCSprite versusScoreSprite = null;
	    protected static CCSprite gol1Sprite = null;
	    protected static CCSprite gol2Sprite = null;
	    
	    protected static int oldMinutes = 100;
	    protected static int oldSeconds = 100;
				
	    protected float scaleFactor = 1;
	    
	    protected BallLayer(){
	    	
	    	super();
	    	
	    	GlobalSingleton.isP1 = true;
	    	GlobalSingleton.isP2 = false;
	    	
	    	this.setIsTouchEnabled(true);
	    	// Enable accelerometer
	    	//setIsAccelerometerEnabled(true);
	    	addBackGroundScene();
	    	setupBox2D();
	    	initGame();
	    	schedule(tickCallback, 1/60);
	    	schedule(updateTimer, 1);
 			
			
	    }
	    private void addBackGroundScene() {
	        CCSprite bgForLevel1 = CCSprite.sprite("screen/field_1.png");
	        bgForLevel1 .setAnchorPoint(0, 0);	        
	      	
	        // Get window size
	    	CGSize s = CCDirector.sharedDirector().winSize();
	    	// Get scale
	    	float scaleX = s.width/bgForLevel1.getContentSize().width;
	        float scaleY = s.height/bgForLevel1.getContentSize().height;
	        // Set for minor scale
	        if(scaleX>scaleY){
	        	scaleFactor = scaleY;
	    	}else{
	        	scaleFactor = scaleX;
	    	}
	        GlobalSingleton.scaleFactor = scaleFactor;
    		bgForLevel1.setScale(scaleFactor);
		    addChild(bgForLevel1 );
	    }
	    
	    protected CCAnimation ballAnim;
	    protected CCAnimate ballAction;
	    protected void initGame(){
	    	GlobalSingleton.timeLeft = 60;
	    	FlurryAgent.logEvent("Init Game");
	    	Log.d(TAG,"create ballAnim");
	    	ArrayList<CCSpriteFrame> frames_ = new ArrayList<>();
	    	String filename = "";
	    	for(int i = 0 ; i<60; i++){
                Log.d(TAG,"add ballAnim FRAME");

                if(i<10){

		    		filename = "ball_000"+i+".png";

	    		}else{
		    		filename = "ball_00"+i+".png";
	    			
	    		}
	    		CCTexture2D tex = CCTextureCache.sharedTextureCache().addImage(filename);
                CGRect rect = CGRect.make(0, 0, 0, 0);
                rect.size = tex.getContentSize();
                CCSpriteFrame frame = CCSpriteFrame.frame(tex, rect, CGPoint.zero());
                frames_.add(frame);

            }

            ballAnim = CCAnimation.animation("ballAnima", frames_);
            ballAction = CCAnimate.action(10.0f, ballAnim, false);
            _ball.addAnimation(ballAnim);


            goal1 = new CCSprite("goal_anima1-hd.png");
	    	goal2 = new CCSprite("goal_anima2-hd.png");
	    	goal3 = new CCSprite("goal_anima3-hd.png");
	    	goal4 = new CCSprite("goal_anima4-hd.png");
	    	goal5 = new CCSprite("goal_anima5-hd.png");
	    	goal6 = new CCSprite("goal_anima6-hd.png");
	    	goal7 = new CCSprite("goal_anima7-hd.png");
	    	goal8 = new CCSprite("goal_anima8-hd.png");
	    	
	    	
	    	touchSprite = new CCSprite("touchSprite.png");
	    	touchSprite.setScale(scaleFactor);
	    	addChild(touchSprite);
	    	
	    	touchSprite.setPosition(10000,10000);
	    	
	    	
	    	GlobalSingleton.teamName1 = "BRA";
	    	GlobalSingleton.teamName2 = "ARG";

            labelPlayer1 = CCLabel.makeLabel(GlobalSingleton.teamName1, "DroidSans", 28*scaleFactor);
            labelPlayer1.setColor(ccColor3B.ccWHITE);
    		labelPlayer1.setPosition(62*scaleFactor,296*scaleFactor);
    		labelPlayer1.setAnchorPoint(0f, 0.5f);
            addChild(labelPlayer1, 306,306);

            labelPlayer2 = CCLabel.makeLabel(GlobalSingleton.teamName2, "DroidSans", 28*scaleFactor);
            labelPlayer2.setColor(ccColor3B.ccWHITE);
    		labelPlayer2.setPosition(416*scaleFactor,296*scaleFactor);
    		labelPlayer2.setAnchorPoint(1f, 0.5f);
            addChild(labelPlayer2, 306,306);
            
            
       
            labelTimerMinutes = CCLabel.makeLabel("00", "DroidSans", 20*scaleFactor);
            labelTimerMinutes.setColor(ccColor3B.ccWHITE);
            labelTimerMinutes.setPosition(238*scaleFactor,298*scaleFactor);
            labelTimerMinutes.setAnchorPoint(1f, 0.5f);
            addChild(labelTimerMinutes, 301,301);
            
            
            CCLabel labelClock = CCLabel.makeLabel(":", "DroidSans", 20*scaleFactor);
            labelClock.setColor(ccColor3B.ccWHITE);
            labelClock.setPosition(240*scaleFactor,298*scaleFactor);
            labelClock.setAnchorPoint(0.5f, 0.5f);
            addChild(labelClock, 301,301);

            labelTimerSeconds = CCLabel.makeLabel("00", "DroidSans", 20*scaleFactor);
            labelTimerSeconds.setColor(ccColor3B.ccWHITE);
            labelTimerSeconds.setPosition(242*scaleFactor,298*scaleFactor);
            labelTimerSeconds.setAnchorPoint(0f, 0.5f);
            addChild(labelTimerSeconds, 301,301);
            

            
            p2Score = CCLabel.makeLabel("0", "DroidSans", 38*scaleFactor);
            p2Score.setColor(ccColor3B.ccWHITE);
            p2Score.setPosition(137*scaleFactor,296*scaleFactor);
            p2Score.setAnchorPoint(0.5f, 0.5f);
            addChild(p2Score, 303,303);
            
            p1Score = CCLabel.makeLabel("0", "DroidSans", 38*scaleFactor);
            p1Score.setColor(ccColor3B.ccWHITE);
            p1Score.setPosition(341*scaleFactor,296*scaleFactor);
            p1Score.setAnchorPoint(0.5f, 0.5f);
            addChild(p1Score, 303,303);
	    	
            player1Sprite = new CCSprite("bra_p1.png");
            player1Sprite.setPosition(180*scaleFactor,298*scaleFactor);
            player1Sprite.setScale(scaleFactor);
            addChild(player1Sprite, 100,300);
            
            player2Sprite = new CCSprite("arg_p2.png");
            player2Sprite.setPosition(302*scaleFactor,298*scaleFactor);
            player2Sprite.setScale(scaleFactor);
            addChild(player2Sprite, 100,300);
            
           
            
            gol1Sprite = new CCSprite("gol1.png");
            gol1Sprite.setPosition(33*scaleFactor,153*scaleFactor);
            gol1Sprite.setScale(scaleFactor);
            addChild(gol1Sprite, 9999,9999);
            
            gol2Sprite = new CCSprite("gol2.png");
            gol2Sprite.setPosition(447*scaleFactor,153*scaleFactor);
            gol2Sprite.setScale(scaleFactor);
            addChild(gol2Sprite, 9998,9998);
         
	    	GlobalSingleton.goal1Scored = false;
	    	GlobalSingleton.goal2Scored = false;
            
	    }
	    
	    
	    
	    
	    protected void goalScored(int team){
	    	
	    	GlobalSingleton.golAnimationPlaying = true;
	    	GlobalSingleton.maxExecutionTime = 0;
	    	if(team==1){
	    		GlobalSingleton.currentScoreP1+=1;
	    		p1Score.setString(""+GlobalSingleton.currentScoreP1);
	    	}else{
	    		GlobalSingleton.currentScoreP2+=1;
	    		p2Score.setString(""+GlobalSingleton.currentScoreP2);	    		
	    	}
	    	this.whistleAudio();
	    	this.crowdGoalAudio();
	    	//GOL ANIMATION
	    	CCAnimation golAnim = CCAnimation.animation("goal");
	    	golAnim.addFrame("goal_anima1-hd.png");
	    	golAnim.addFrame("goal_anima2-hd.png");
	    	golAnim.addFrame("goal_anima3-hd.png");
	    	golAnim.addFrame("goal_anima4-hd.png");
	    	golAnim.addFrame("goal_anima5-hd.png");
	    	golAnim.addFrame("goal_anima6-hd.png");
	    	golAnim.addFrame("goal_anima7-hd.png");
	    	golAnim.addFrame("goal_anima1-hd.png");
	    	golAnim.addFrame("goal_anima2-hd.png");
	    	golAnim.addFrame("goal_anima3-hd.png");
	    	golAnim.addFrame("goal_anima4-hd.png");
	    	golAnim.addFrame("goal_anima5-hd.png");
	    	golAnim.addFrame("goal_anima6-hd.png");
	    	golAnim.addFrame("goal_anima7-hd.png");
	    	golAnim.addFrame("goal_anima1-hd.png");
	    	golAnim.addFrame("goal_anima2-hd.png");
	    	golAnim.addFrame("goal_anima3-hd.png");
	    	golAnim.addFrame("goal_anima4-hd.png");
	    	golAnim.addFrame("goal_anima5-hd.png");
	    	golAnim.addFrame("goal_anima6-hd.png");
	    	golAnim.addFrame("goal_anima7-hd.png");
	    	golAnim.addFrame("goal_anima8-hd.png");
	    	
	    	CCAnimate golAction = CCAnimate.action(2.0f, golAnim, false);
	    	CCSprite golSprite = new CCSprite("goal_anima8-hd.png");
	    	golSprite.setPosition( CGPoint.make(240*scaleFactor, 160*scaleFactor));
	    	this.addChild(golSprite,10000,100);
	    	golSprite.runAction(golAction);
	    	
	    	
	    	unschedule(startPlayerTurn);
	    	GlobalSingleton.timeToDelayAction = 4;
	    	schedule(startDelayRestart, 1 );
	    }
	    
	    
	    
	    
	    
	    
		///////////////////////////////////////////////////////
		//a       x           x  =   a    * c
		//----- = -----              -----
		//b       c                  b
		///////////////////////////////////////////////////////
	    
	/*	private int ruleOfThree(int a,int b, int c){
			int x = (a/b)* c;
			return x;
		} 

		CCSpriteSheet _batchNode;
	    private void setupBatchNode(){
	    	String spritesheetPvrCcz = "spritesheet.pvr.ccz";
	    	String spritesheetPlist = "spritesheet.plist";
	    	_batchNode =  CCSpriteSheet.spriteSheet(spritesheetPvrCcz);
	    }
	    */
		public static CCScene scene()
		{
			CCScene scene = CCScene.node();
			CCLayer layer = new BallLayer();
			scene.addChild(layer);
			return scene;
		}

    private static float DENSITY = 1.0f;
    private static float FRICTION = 1.0f;  //0.6
    private static float RESTITUTION = 1.0f;  //0.5

		public void setupBox2D() {
			
	    	// Get window size
	    	CGSize s = CCDirector.sharedDirector().winSize();
	    	// Use scaled width and height so that our edges always match the current screen
	    	float scaledWidth = s.width/PTM_RATIO;
	        float scaledHeight = s.height/PTM_RATIO;
	    	// Create a world
	        //Vector2 gravity = new Vector2(0.0f, -30.0f);
	        Vector2 gravity = new Vector2(0.0f, 0.0f);
	    	//boolean doSleep = true;
	    	boolean doSleep = false;
	    	_world = new World(gravity, doSleep);
	    	ContactListener  _contactListener = new GameContactListener();
	    	
	    	_world.setContactListener(_contactListener);
	    	
	    	// Create edges around the entire screen
	    	// Define the ground body.
	        BodyDef bxGroundBodyDef = new BodyDef();
	        bxGroundBodyDef.position.set(0.0f, 0.0f);
			
			// The body is also added to the world.
	        Body groundBody = _world.createBody(bxGroundBodyDef);
	
	        // Define the ground box shape.
	        PolygonShape groundBox = new PolygonShape();
	
	        Vector2 bottomLeft = new Vector2(0f,0f);
	        Vector2 topLeft = new Vector2(0f,scaledHeight);
	       // Vector2 topRight = new Vector2(scaledWidth,scaledHeight);
	      //  Vector2 bottomRight = new Vector2(scaledWidth,0f);
	       /* 
	        Log.e(TAG, "scaledWidth "+scaledWidth);
	        Log.e(TAG, "scaledHeight "+scaledHeight);
	        Log.e(TAG, "PTM_RATIO "+PTM_RATIO);
	        Log.e(TAG, "scaleFactor "+scaleFactor);
	        Log.e(TAG, "scaleFactor/PTM_RATIO "+scaleFactor/PTM_RATIO);
			*/
	       // CCSprite _ip = CCSprite.sprite("invisible-pixel.png");
	    	//_ip.setTag(1000);
			// bottom
	        groundBox.setAsEdge(new Vector2(0, 27*scaleFactor/PTM_RATIO), new Vector2(scaledWidth, 27*scaleFactor/PTM_RATIO));
	        groundBody.setUserData("bottom");
			groundBody.createFixture(groundBox,0);
			
			// top
			groundBox.setAsEdge(new Vector2(0, 276*scaleFactor/PTM_RATIO), new Vector2(scaledWidth, 276*scaleFactor/PTM_RATIO));
			groundBody.setUserData("top");
			groundBody.createFixture(groundBox,0);
			
			Body body;
			BodyDef bodyGoalDef= new BodyDef();
			PolygonShape boxShape = new PolygonShape();
			
			//filter.groupIndex = 0;
			
			
			// adding the goal left
			bodyGoalDef.position.set(33*scaleFactor/PTM_RATIO, 154*scaleFactor/PTM_RATIO);
			boxShape.setAsBox(33*scaleFactor/PTM_RATIO, 50*scaleFactor/PTM_RATIO);
			body=_world.createBody(bodyGoalDef);
			FixtureDef goalShapeDef = new FixtureDef();
			goalShapeDef.filter.groupIndex = -2;
			goalShapeDef.shape = boxShape;
			goalShapeDef.density = 0.0f;
			goalShapeDef.friction = 0.1f; 
			goalShapeDef.restitution = 0.4f;
			body.createFixture(goalShapeDef);
			
			// adding the goal left
			BodyDef bodyGoalSensorDef= new BodyDef();
			bodyGoalSensorDef.position.set(23*scaleFactor/PTM_RATIO, 154*scaleFactor/PTM_RATIO);
			boxShape.setAsBox(22*scaleFactor/PTM_RATIO, 50*scaleFactor/PTM_RATIO);
			body=_world.createBody(bodyGoalSensorDef);
			body.setUserData("goal1");
			FixtureDef goalSensorShapeDef = new FixtureDef();
			goalSensorShapeDef.isSensor = true;
			goalSensorShapeDef.shape = boxShape;
			goalSensorShapeDef.density = 0.0f;
			goalSensorShapeDef.friction = 0.1f; 
			goalSensorShapeDef.restitution = 0.4f;
			body.createFixture(goalSensorShapeDef);
			
			// left gol 1
			//------
			groundBox.setAsEdge(new Vector2(0,104*scaleFactor/PTM_RATIO), new Vector2(66*scaleFactor/PTM_RATIO,104*scaleFactor/PTM_RATIO));
			groundBody.setUserData("ground");
			groundBody.createFixture(groundBox,0);
			// left corner 1
			// |
			groundBox.setAsEdge(new Vector2(66*scaleFactor/PTM_RATIO,0), new Vector2(66*scaleFactor/PTM_RATIO,104*scaleFactor/PTM_RATIO));
			groundBody.setUserData("ground");
			groundBody.createFixture(groundBox,0);
				
			// left gol 2
			//-------
			groundBox.setAsEdge(new Vector2(0,204*scaleFactor/PTM_RATIO), new Vector2(66*scaleFactor/PTM_RATIO,204*scaleFactor/PTM_RATIO));
			groundBody.setUserData("ground");
			groundBody.createFixture(groundBox,0);
			// left corner 2
			// |
			groundBox.setAsEdge(new Vector2(66*scaleFactor/PTM_RATIO,204*scaleFactor/PTM_RATIO), new Vector2(66*scaleFactor/PTM_RATIO,304*scaleFactor/PTM_RATIO));
			groundBody.setUserData("ground");
			groundBody.createFixture(groundBox,0);
									
			
			// adding the goal right
			bodyGoalDef.position.set(449*scaleFactor/PTM_RATIO, 154*scaleFactor/PTM_RATIO);
			boxShape.setAsBox(33*scaleFactor/PTM_RATIO, 50*scaleFactor/PTM_RATIO);
			body=_world.createBody(bodyGoalDef);
			FixtureDef goalShapeDef2 = new FixtureDef();
			goalShapeDef2.filter.groupIndex = -2;
			goalShapeDef2.shape = boxShape;
			goalShapeDef2.density = 0.0f;
			goalShapeDef2.friction = 0.1f; 
			goalShapeDef2.restitution = 0.4f;
			body.createFixture(goalShapeDef2);
			
			bodyGoalSensorDef.position.set(459*scaleFactor/PTM_RATIO, 154*scaleFactor/PTM_RATIO);
			boxShape.setAsBox(22*scaleFactor/PTM_RATIO, 50*scaleFactor/PTM_RATIO);
			body=_world.createBody(bodyGoalSensorDef);
			body.setUserData("goal2");
			FixtureDef goalSensorShapeDef2 = new FixtureDef();
			goalSensorShapeDef2.isSensor = true;
			goalSensorShapeDef2.shape = boxShape;
			goalSensorShapeDef2.density = 0.0f;
			goalSensorShapeDef2.friction = 0.1f; 
			goalSensorShapeDef2.restitution = 0.4f;
			body.createFixture(goalSensorShapeDef2);
			
						
			// right gol 1
			//------
			groundBox.setAsEdge(new Vector2(416*scaleFactor/PTM_RATIO,104*scaleFactor/PTM_RATIO), new Vector2(480*scaleFactor/PTM_RATIO,104*scaleFactor/PTM_RATIO));
			groundBody.setUserData("ground");
			groundBody.createFixture(groundBox,0);
			// right corner 1
			// |
			groundBox.setAsEdge(new Vector2(416*scaleFactor/PTM_RATIO,0), new Vector2(416*scaleFactor/PTM_RATIO,104*scaleFactor/PTM_RATIO));
			groundBody.setUserData("ground");
			groundBody.createFixture(groundBox,0);
						
			// right gol 2
			//--------
			groundBox.setAsEdge(new Vector2(416*scaleFactor/PTM_RATIO,204*scaleFactor/PTM_RATIO), new Vector2(480*scaleFactor/PTM_RATIO,204*scaleFactor/PTM_RATIO));
			groundBody.setUserData("ground");
			groundBody.createFixture(groundBox,0);
			// right corner 2
			// |
			groundBox.setAsEdge(new Vector2(416*scaleFactor/PTM_RATIO,204*scaleFactor/PTM_RATIO), new Vector2(416*scaleFactor/PTM_RATIO,304*scaleFactor/PTM_RATIO));
			groundBody.setUserData("ground");
			groundBody.createFixture(groundBox,0);
						
			/*
			 
		Y	 (0 ,scaledHeight)   -------------- (scaledWidth ,scaledHeight)
		|	 *         | c2                  c3 |         * 
		|	 *    2    |                        |   2     *
		|	 * --------(66,204)        (420,204)--------- *
		|	 *                                            *
		|	 * --------(66,104)         (420,104) -------- *
		|	 *    1    |                        |   1     *
		|	 *         | c1                  c4 |         *
		|	 (0,0)  --------------------------- (scaledWidth ,0)
		|	  
		|	  
	 	-------------------------X
	 	Corner 1 - c1(66,0)(70,4)
	 	Corner 2 - c2(66,scaleHeight-4)(70,scaleHight)
	 	Corner 3 - c3(416,scaleHeight-4)(420,scaleHight)
	 	Corner 4 - c4(scaledWidth-4,0)(scaleWidth,4)
	 	*/
			
			
		// CORNER 1
		//------
		groundBox.setAsEdge(new Vector2(0,104*scaleFactor/PTM_RATIO), new Vector2(70*scaleFactor/PTM_RATIO,104*scaleFactor/PTM_RATIO));
		groundBody.setUserData("corner1");
		groundBody.createFixture(groundBox,0);	
			
			
		    
			// left 
			groundBox.setAsEdge(topLeft, bottomLeft);
			groundBody.setUserData("ground");
			groundBody.createFixture(groundBox,0);

			// right
			groundBox.setAsEdge(new Vector2(480*scaleFactor/PTM_RATIO,0), new Vector2(480*scaleFactor/PTM_RATIO,scaledHeight));
			groundBody.setUserData("ground");
			groundBody.createFixture(groundBox,0);
	    	
			/*********************************************/
	    	
			// Create sprite and add it to the layer
	    	_ball = CCSprite.sprite("ball.png");
	    	_ball.setTag(1000);
	    	_ball.setScale(scaleFactor);
	    	_ball.setPosition(CGPoint.make(100*scaleFactor, 100*scaleFactor));
	    	this.addChild(_ball);
	    
			
	    	// Create ball body and shape	    	
	    	BodyDef ballBodyDef = new BodyDef();
	    	ballBodyDef.type = BodyType.DynamicBody;
	    	ballBodyDef.position.set( 240 * scaleFactor  / PTM_RATIO,  153 * scaleFactor  / PTM_RATIO);
	    	ballBodyDef.linearDamping = 0.6f;
	    	ballBodyDef.angularDamping = 0.6f;
	    	
	    	ballBody = _world.createBody(ballBodyDef);
	    	ballBody.setUserData(_ball);
	    	CircleShape circle = new CircleShape();
	    	circle.setRadius(12.0f*scaleFactor/PTM_RATIO);
	  	
	    	FixtureDef ballFixtureDef = new FixtureDef();
	    	ballFixtureDef.filter.groupIndex = -2;
			
	    	//ballFixtureDef.filter.categoryBits = 0x0010;
	    	//ballFixtureDef.filter.maskBits = 4;
	    	
	    	
	    	ballFixtureDef.shape = circle;
	    	ballFixtureDef.density = 0.7f;
	    	ballFixtureDef.friction = 0.1f;
	    	ballFixtureDef.restitution = 0.1f;	  

//	    	ballShapeDef.shape = circle;
//	    	ballShapeDef.density = 1.0f;
//	    	ballShapeDef.friction = 0.1f; 
//	    	ballShapeDef.restitution = 0.5f;	  

	    	
	    	ballBody.createFixture(ballFixtureDef); 
	    	ballBody.resetMassData();
	    	/*********************************************/
	    	
			// Create sprite and add it to the layer
	    	_p1 = CCSprite.sprite("bra_p1.png");
	    	_p1.setTag(1001);
	    	_p1.setScale(scaleFactor);
	    	_p1.setPosition(CGPoint.make( 100 * scaleFactor, 100 * scaleFactor));
	    	this.addChild(_p1);
	    	_p1r = CCSprite.sprite("bra_p1.png");
	    	_p1r.setTag(1);
	    	_p1r.setPosition(CGPoint.make(_p1r.getContentSize().width/2, _p1r.getContentSize().height/2));
	    	_p1.addChild(_p1r);
	    	CCSprite arrowSprite = new CCSprite("drag_arrow.png");
			arrowSprite.setPosition(_p1.getContentSize().width/2, _p1.getContentSize().height/2);
			arrowSprite.setAnchorPoint(0.1f, 0.5f);	
			_p1.addChild(arrowSprite,-1,201);
			arrowSprite.setScaleX(0.1f);
			
			
			
	    	// Create ball body and shape	    	
	    	BodyDef p1BodyDef = new BodyDef();
	    	p1BodyDef.type = BodyType.DynamicBody;
	    	p1BodyDef.linearDamping = 0.8f;
	    	p1BodyDef.angularDamping = 0.8f;
	    	p1BodyDef.position.set( 140 * scaleFactor / PTM_RATIO, 240 * scaleFactor / PTM_RATIO);
	    	p1Body = _world.createBody(p1BodyDef);
	    	p1Body.setUserData(_p1);
	    	CircleShape p1Shape = new CircleShape();
	    	p1Shape.setRadius(18.0f*scaleFactor/PTM_RATIO);
	  	
	    	FixtureDef p1ShapeDef = new FixtureDef();
	    	p1ShapeDef.shape = p1Shape;
	    	p1ShapeDef.density = DENSITY;
	    	p1ShapeDef.friction = FRICTION;
	    	p1ShapeDef.restitution = RESTITUTION;
	    	p1Body.createFixture(p1ShapeDef); 
	    	p1Body.resetMassData();
	    	/*********************************************/
			// Create sprite and add it to the layer
	    	_p2 = CCSprite.sprite("bra_p1.png");
	    	_p2.setTag(1002);
	    	_p2.setScale(scaleFactor);
	    	_p2.setPosition(CGPoint.make(100*scaleFactor, 100*scaleFactor));
	    	this.addChild(_p2);
	    	_p2r = CCSprite.sprite("bra_p1.png");
	    	_p2r.setTag(1);
	    	_p2r.setPosition(CGPoint.make(_p2r.getContentSize().width/2, _p2r.getContentSize().height/2));
	    	_p2.addChild(_p2r);
	    	arrowSprite = new CCSprite("drag_arrow.png");
			arrowSprite.setPosition(_p2.getContentSize().width/2, _p2.getContentSize().height/2);
			arrowSprite.setAnchorPoint(0.1f, 0.5f);	
			_p2.addChild(arrowSprite,-1,201);
			arrowSprite.setScaleX(0.1f);
			
			// Create ball body and shape	    	
	    	BodyDef p2BodyDef = new BodyDef();
	    	p2BodyDef.type = BodyType.DynamicBody;
	    	p2BodyDef.linearDamping = 0.8f;
	    	p2BodyDef.angularDamping = 0.8f;
	    	p2BodyDef.position.set(100*scaleFactor/PTM_RATIO, 153*scaleFactor/PTM_RATIO);
	    	p2Body = _world.createBody(p2BodyDef);
	    	p2Body.setUserData(_p2);
	    	CircleShape p2Shape = new CircleShape();
	    	p2Shape.setRadius(18.0f*scaleFactor/PTM_RATIO);
	  	
	    	FixtureDef p2ShapeDef = new FixtureDef();
	    	p2ShapeDef.shape = p1Shape;
	    	p2ShapeDef.density = DENSITY;
	    	p2ShapeDef.friction = FRICTION;
	    	p2ShapeDef.restitution = RESTITUTION;
	    	p2Body.createFixture(p2ShapeDef); 
	    	p2Body.resetMassData();
	    	
	    	/*********************************************/
	    	
			// Create sprite and add it to the layer
	    	_p3 = CCSprite.sprite("bra_p1.png");
	    	_p3.setTag(1003);
	    	_p3.setScale(scaleFactor);
	    	_p3.setPosition(CGPoint.make(100*scaleFactor, 100*scaleFactor));
	    	this.addChild(_p3);
	    	_p3r = CCSprite.sprite("bra_p1.png");
	    	_p3r.setTag(1);
	    	_p3r.setPosition(CGPoint.make(_p3r.getContentSize().width/2, _p3r.getContentSize().height/2));
	    	_p3.addChild(_p3r);
	    	arrowSprite = new CCSprite("drag_arrow.png");
			arrowSprite.setPosition(_p3.getContentSize().width/2, _p3.getContentSize().height/2);
			arrowSprite.setAnchorPoint(0.1f, 0.5f);	
			_p3.addChild(arrowSprite,-1,201);
			arrowSprite.setScaleX(0.1f);
			
	    	// Create ball body and shape	    	
	    	BodyDef p3BodyDef = new BodyDef();
	    	p3BodyDef.type = BodyType.DynamicBody;
	    	p3BodyDef.linearDamping = 0.8f;
	    	p3BodyDef.angularDamping = 0.8f;
	    	p3BodyDef.position.set(140*scaleFactor/PTM_RATIO, 66*scaleFactor/PTM_RATIO);
	    	p3Body = _world.createBody(p3BodyDef);
	    	p3Body.setUserData(_p3);
	    	CircleShape p3Shape = new CircleShape();
	    	p3Shape.setRadius(18.0f*scaleFactor/PTM_RATIO);
	  	
	    	FixtureDef p3ShapeDef = new FixtureDef();
	    	p3ShapeDef.shape = p3Shape;
	    	p3ShapeDef.density = DENSITY;
	    	p3ShapeDef.friction = FRICTION;
	    	p3ShapeDef.restitution = RESTITUTION;
	    	p3Body.createFixture(p3ShapeDef); 
	    	p3Body.resetMassData();
	    	
	    	/*********************************************/
	    	
			// Create sprite and add it to the layer
	    	_p4 = CCSprite.sprite("arg_p2.png");
	    	_p4.setTag(1004);
	    	_p4.setScale(scaleFactor);
	    	_p4.setPosition(CGPoint.make(100*scaleFactor, 100*scaleFactor));
	    	this.addChild(_p4);
	    	_p4r = CCSprite.sprite("arg_p2.png");
	    	_p4r.setTag(1);
	    	_p4r.setPosition(CGPoint.make(_p4r.getContentSize().width/2, _p4r.getContentSize().height/2));
	    	_p4.addChild(_p4r);
	    	arrowSprite = new CCSprite("drag_arrow.png");
			arrowSprite.setPosition(_p4.getContentSize().width/2, _p4.getContentSize().height/2);
			arrowSprite.setAnchorPoint(0.1f, 0.5f);	
			_p4.addChild(arrowSprite,-1,201);
			arrowSprite.setScaleX(0.1f);

	    	// Create ball body and shape	    	
	    	BodyDef p4BodyDef = new BodyDef();
	    	p4BodyDef.type = BodyType.DynamicBody;
	    	p4BodyDef.linearDamping = 0.8f;
	    	p4BodyDef.angularDamping = 0.8f;
	    	p4BodyDef.position.set(320*scaleFactor/PTM_RATIO, 240*scaleFactor/PTM_RATIO);
	    	p4Body = _world.createBody(p4BodyDef);
	    	p4Body.setUserData(_p4);
	    	CircleShape p4Shape = new CircleShape();
	    	p4Shape.setRadius(18.0f*scaleFactor/PTM_RATIO);
	  	
	    	FixtureDef p4ShapeDef = new FixtureDef();
	    	p4ShapeDef.shape = p4Shape;
	    	p4ShapeDef.density = DENSITY;
	    	p4ShapeDef.friction = FRICTION;
	    	p4ShapeDef.restitution = RESTITUTION;
	    	p4Body.createFixture(p4ShapeDef); 
	    	p4Body.resetMassData();
	    	
	    	/*********************************************/
	    	
			// Create sprite and add it to the layer
	    	_p5 = CCSprite.sprite("arg_p2.png");
	    	_p5.setTag(1005);
	    	_p5.setScale(scaleFactor);
	    	_p5.setPosition(CGPoint.make(100*scaleFactor, 100*scaleFactor));
	    	this.addChild(_p5);
	    	_p5r = CCSprite.sprite("arg_p2.png");
	    	_p5r.setTag(1);
	    	_p5r.setPosition(CGPoint.make(_p5r.getContentSize().width/2, _p5r.getContentSize().height/2));
	    	_p5.addChild(_p5r);
	    	arrowSprite = new CCSprite("drag_arrow.png");
			arrowSprite.setPosition(_p5.getContentSize().width/2, _p5.getContentSize().height/2);
			arrowSprite.setAnchorPoint(0.1f, 0.5f);	
			_p5.addChild(arrowSprite,-1,201);
			arrowSprite.setScaleX(0.1f);
			
	    	// Create ball body and shape	    	
	    	BodyDef p5BodyDef = new BodyDef();
	    	p5BodyDef.type = BodyType.DynamicBody;
	    	p5BodyDef.linearDamping = 0.8f;
	    	p5BodyDef.angularDamping = 0.8f;
	    	p5BodyDef.position.set(380*scaleFactor/PTM_RATIO, 153*scaleFactor/PTM_RATIO);
	    	p5Body = _world.createBody(p5BodyDef);
	    	p5Body.setUserData(_p5);
	    	CircleShape p5Shape = new CircleShape();
	    	p5Shape.setRadius(18.0f*scaleFactor/PTM_RATIO);
	  	
	    	FixtureDef p5ShapeDef = new FixtureDef();
	    	p5ShapeDef.shape = p5Shape;
	    	p5ShapeDef.density = DENSITY;
	    	p5ShapeDef.friction = FRICTION;
	    	p5ShapeDef.restitution = RESTITUTION;
	    	p5Body.createFixture(p5ShapeDef); 
	    	p5Body.resetMassData();
	    	
	    	/*********************************************/
	    		    	
	    	
			// Create sprite and add it to the layer
	    	_p6 = CCSprite.sprite("arg_p2.png");
	    	_p6.setTag(1006);
	    	_p6.setScale(scaleFactor);
	    	_p6.setPosition(CGPoint.make(100*scaleFactor, 100*scaleFactor));
	    	this.addChild(_p6);
	    	_p6r = CCSprite.sprite("arg_p2.png");
	    	_p6r.setTag(1);
	    	_p6r.setPosition(CGPoint.make(_p6r.getContentSize().width/2, _p6r.getContentSize().height/2));
	    	_p6.addChild(_p6r);
	    	arrowSprite = new CCSprite("drag_arrow.png");
			arrowSprite.setPosition(_p6.getContentSize().width/2, _p6.getContentSize().height/2);
			arrowSprite.setAnchorPoint(0.1f, 0.5f);	
			_p6.addChild(arrowSprite,-1,201);
			arrowSprite.setScaleX(0.1f);
			
	    	// Create ball body and shape	    	
	    	BodyDef p6BodyDef = new BodyDef();
	    	p6BodyDef.type = BodyType.DynamicBody;
	    	p6BodyDef.linearDamping = 0.8f;
	    	p6BodyDef.angularDamping = 0.8f;
	    	p6BodyDef.position.set(340*scaleFactor/PTM_RATIO, 66*scaleFactor/PTM_RATIO);
	    	p6Body = _world.createBody(p6BodyDef);
	    	p6Body.setUserData(_p6);
	    	CircleShape p6Shape = new CircleShape();
	    	p6Shape.setRadius(18.0f*scaleFactor/PTM_RATIO);
	  	
	    	FixtureDef p6ShapeDef = new FixtureDef();
	    	p6ShapeDef.shape = p5Shape;
	    	p6ShapeDef.density = DENSITY;
	    	p6ShapeDef.friction = FRICTION;
	    	p6ShapeDef.restitution = RESTITUTION;
	    	p6Body.createFixture(p6ShapeDef); 
	    	p6Body.resetMassData();
	    	
	    	


	    	//CORNER BOTTOM LEFT
//	    	cpBody* cornerBody1 = cpBodyNew(INFINITY, INFINITY);
//	    	cornerBody1->p = cpv(70, 27);
//	    	//cpSpaceAddBody(space, cornerBody1);
//	    	cpShape* cornerShapeBottomLeft = cpCircleShapeNew(cornerBody1, 20, cpvzero);
//	    	if([[GlobalSingleton sharedInstance] gameType]==2){
//	            cornerShapeBottomLeft->e = 1;
//	            cornerShapeBottomLeft->u = 1;
//	        }else{
//	            cornerShapeBottomLeft->e = 0.5;
//	            cornerShapeBottomLeft->u = 0.1;
//	    	}
//	    	cornerShapeBottomLeft->data = cornerSprite1;
//	    	cornerShapeBottomLeft->collision_type = 1;
//	    	cpSpaceAddStaticShape(space, cornerShapeBottomLeft);
//	    	
//	    	
//	    	//CORNER TOP LEFT
//	    	cpBody* cornerBody2 = cpBodyNew(INFINITY, INFINITY);
//	    	cornerBody2->p = cpv(70, 273);
////	    	cpSpaceAddBody(space, cornerBody2);
//	    	cpShape* cornerShapeTopLeft = cpCircleShapeNew(cornerBody2, 20, cpvzero);
//	    	if([[GlobalSingleton sharedInstance] gameType]==2){
//	            cornerShapeTopLeft->e = 1;
//	            cornerShapeTopLeft->u = 1;
//	        }else{
//	            cornerShapeTopLeft->e = 0.5;
//	            cornerShapeTopLeft->u = 0.1;
//	    	};
//	    	cornerShapeTopLeft->data = cornerSprite2;
//	    	cornerShapeTopLeft->collision_type = 1;
//	    	cpSpaceAddStaticShape(space, cornerShapeTopLeft);
//	    	
//	    	
//	    	//CORNER BOTTOM RIGHT
//	    	cpBody* cornerBody3 = cpBodyNew(INFINITY, INFINITY);
//	    	cornerBody3->p = cpv(410, 27);
////	    	cpSpaceAddBody(space, cornerBody3);
//	    	cpShape* cornerShapeBottomRight = cpCircleShapeNew(cornerBody3, 20, cpvzero);
//	    	cpBodySetAngle(cornerBody3,-0.7853);
//	    	if([[GlobalSingleton sharedInstance] gameType]==2){
//	            cornerShapeBottomRight->e = 1;
//	            cornerShapeBottomRight->u = 1;
//	        }else{
//	            cornerShapeBottomRight->e = 0.5;
//	            cornerShapeBottomRight->u = 0.1;
//	    	}
//	    	cornerShapeBottomRight->data = cornerSprite3;
//	    	cornerShapeBottomRight->collision_type = 1;
//	    	cpSpaceAddStaticShape(space, cornerShapeBottomRight);
//	    	
//	    	//CORNER TOP LEFT
//	    	cpBody* cornerBody4 = cpBodyNew(INFINITY, INFINITY);
//	    	cornerBody4->p = cpv(410, 273);
//	    	cpBodySetAngle(cornerBody4,0.7853);
////	    	cpSpaceAddBody(space, cornerBody4);
//	    	cpShape* cornerShapeTopRight = cpCircleShapeNew(cornerBody4, 20, cpvzero);
//	    	if([[GlobalSingleton sharedInstance] gameType]==2){
//	            cornerShapeTopRight->e = 1;
//	            cornerShapeTopRight->u = 1;
//	        }else{
//	            cornerShapeTopRight->e = 0.5;
//	            cornerShapeTopRight->u = 0.1;
//	    	}
//	    	cornerShapeTopRight->data = cornerSprite4;
//	    	cornerShapeTopRight->collision_type = 1;
//	    	cpSpaceAddStaticShape(space, cornerShapeTopRight);

	    	
	    	
	    	
	    	
	    	
	    	
	    	/*********************************************/
	    		    	
	    	Log.e("BOX2d","teste"+GlobalSingleton.teste);
	    	_world.clearForces();
		}
		
		
		protected double diffPosx;
		protected double diffPosy;
		protected double oldpx;
		protected double oldpy;
		protected int ballFrameNumber;
		
		protected void ball3Danimation(){
			double b = ballBody.getLinearVelocity().x;
			double c = ballBody.getLinearVelocity().y;
			int degreeAngle = (int) (Math.atan2(b,c) * 180 / 3.14565656);
			
			float degreesDiff =  (float) (( degreeAngle+90 - _ball.getRotation() )*0.1+_ball.getRotation());
			_ball.setRotation(-degreeAngle+90);
			 diffPosx = ((_ball.getPosition().x - oldpx)*100 )+ diffPosx;
			 diffPosy = ((_ball.getPosition().y - oldpy)*100 )+ diffPosy;
			 double diffMax = 60.0f;
			 
			 double diff = Math.sqrt(Math.pow(diffPosx,2.0d)+Math.pow(diffPosy,2.0d));
			 
			 if( (diff > diffMax || diff < -diffMax) ){
			        if (diff > Math.abs(diffMax)) {
			            ballFrameNumber = (int) (ballFrameNumber+(1*diff/60)<59?ballFrameNumber+(1*diff/60):1+(1*diff/60));
			        }else{
			            ballFrameNumber = ballFrameNumber+1<59?ballFrameNumber+1:0;
			        }
			        Log.d(TAG,""+ballFrameNumber);
			        ballFrameNumber = ballFrameNumber<=59?ballFrameNumber:0;
					 Log.d(TAG,""+ballFrameNumber);
			        String filename;
			        if(ballFrameNumber<10){
			        	filename = "ball_000"+ballFrameNumber+".png";
			         }else{
			        	filename = "ball_00"+ballFrameNumber+".png"; 
			        }

				    //Log.i(TAG,"frame:"+ballFrameNumber);
			        if(ballAnim!=null && !ballAnim.frames().isEmpty()) {
                        _ball.setDisplayFrame("ballAnima", ballFrameNumber);
                    }
				 // [ballSprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"ball_%04d.png", ballFrameNumber]]];

				 	//Context context = CCDirector.sharedDirector().getActivity();
					//CCTexture2D texture = new CCTexture2D();
					//texture.initWithImage(getBitmapFromAsset(context,filename));
				   //_ball.setDisplayFrame(CCSpriteFrame.frame(texture, CGRect.make(CGPoint.ccp(0,0),CGSize.make(texture.getWidth(),texture.getHeight())),CGPoint.ccp(0,0)));
			        diffPosy = 0.0f;
			        diffPosx = 0.0f;
			    }
			 
			 oldpy = _ball.getPosition().y;
			 oldpx = _ball.getPosition().x;
//			    ballSpriteShadow.position = ballSprite.position;
//			    ballSpriteVolume.position = ballSprite.position;
		}


//		-(void)ball3Danimation{
//		    //BALL ROLLING
//		    float b = (ballBody->v.y);
//		    float c = (ballBody->v.x);
//		    int degreeAngle = atan2(b,c) * 180 / 3.14565656;
//		    //float degreesDiff =  ( degreeAngle+90 - ballSprite.rotation )*0.1+ballSprite.rotation;
//		    [ballSprite setRotation:-degreeAngle+90];
//
//		    //TODO implement 1 decimal to the animation
//		    // NSString* fnOldpy = [NSString stringWithFormat:@"%.1f", oldpy];
//		    //NSString* fnpy = [NSString stringWithFormat:@"%.1f", ballSprite.position.y];
//
//		    diffPosx = ((ballSprite.position.x - oldpx)*100 )+ diffPosx;
//		    diffPosy = ((ballSprite.position.y - oldpy)*100 )+ diffPosy;
//		    float diffMax = 60.0f;
//		   // NSLog(@"diff pos: %f", diffPosy);
//
//		    float diff = sqrtf(powf(diffPosx,2.0f)+powf(diffPosy,2.0f));
//
//		    //NSLog(@"FLOAT DIFF %f",diff);
//
//		    if( (diff > diffMax || diff < -diffMax) ){
//		        if (diff > fabs(diffMax)) {
//		      //      NSLog(@"DIFFERENCA: %i",(1*int(diff)));
//		            ballFrameNumber = ballFrameNumber+(1*int(diff)/60)<59?ballFrameNumber+(1*int(diff)/60):1+(1*int(diff)/60);
//		        }else{
//		            ballFrameNumber = ballFrameNumber+1<59?ballFrameNumber+1:0;
//		        }
//		        ballFrameNumber = ballFrameNumber<=59?ballFrameNumber:0;
//		        [ballSprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"ball_%04d.png", ballFrameNumber]]];
//		        diffPosy = 0.0f;
//		        diffPosx = 0.0f;
//		    }
//		    //NSLog(@"a CC_RADIANS:%f  body->a:%g",(float) CC_RADIANS_TO_DEGREES( -body->a ),(double)body->a);
//		    oldpy = ballSprite.position.y;
//		    oldpx = ballSprite.position.x;
//		    ballSpriteShadow.position = ballSprite.position;
//		    ballSpriteVolume.position = ballSprite.position;
//		}
		
		private UpdateCallback updateTimer = new UpdateCallback() {
			
			@Override
			public void update(float d) {
				tickUpdateTimer(d);
			}
		};
		public synchronized void tickUpdateTimer(float delta) {
			//Log.i(TAG,"Entered updateTimer() : "+GlobalSingleton.timeLeft + ":"+ GlobalSingleton.golAnimationPlaying);
//	    	synchronized (_world) {
//	    		_world.step(delta, 8, 1);
//	    	}
			ball3Danimation();
	    	//GlobalSingleton.
	    	
	    	if(GlobalSingleton.goal1Scored==true){
	    		//Log.i(TAG,"GOAL SCORED 1 : "+GlobalSingleton.goal1Scored);
		    	this.goalScored(1);
	    		GlobalSingleton.goal1Scored=false;
	    	}
	    	if(GlobalSingleton.goal2Scored==true){
	    		//Log.i(TAG,"GOAL SCORED 2 : "+GlobalSingleton.goal2Scored);
	    		this.goalScored(2);
	    		GlobalSingleton.goal2Scored=false;
	    	}
	    	if(GlobalSingleton.timeLeft==60){
	    		this.whistleAudio();
	    	}
	    	if(GlobalSingleton.timeLeft==30 || GlobalSingleton.timeLeft==50 || GlobalSingleton.timeLeft==200 || GlobalSingleton.timeLeft==300 || GlobalSingleton.timeLeft==400 || GlobalSingleton.timeLeft==500 || GlobalSingleton.timeLeft==600 ){
	    		this.gameCrowdAudio();
	    	}
	    	if(GlobalSingleton.timeLeft==2){
	    		this.whistleAudio();
	    	}
	    	
	    	if(GlobalSingleton.timeLeft>0){
	    	//	Log.i(TAG,"TIME LEFT : "+GlobalSingleton.timeLeft);
	    		
	    		GlobalSingleton.timeLeft = GlobalSingleton.timeLeft - 1;
				int totalSeconds = GlobalSingleton.timeLeft;

				int minutes = totalSeconds/60;
				int seconds = totalSeconds%60;
				String minutesString;
				String secondsString;
				if(minutes<=9){
					minutesString = "0"+minutes;
				}else{
					minutesString = ""+minutes;
				}
				if(seconds<=9){
					secondsString = "0"+seconds;
				}else{
					secondsString = ""+seconds;
				}
				
				if(oldMinutes != minutes){
					
		           // labelTimerMinutes.scale = 0.01;
		            CCScaleTo rot1 = CCScaleTo.action( 0.03f ,1.2f); 
		            CCScaleTo rot2 = CCScaleTo.action( 0.07f , 1.0f); 
		            CCSequence seq = CCSequence.actions(rot1, rot2);
		            labelTimerMinutes.runAction(seq);
		            labelTimerMinutes.setString(minutesString);
		        }
				
		        if(oldSeconds != seconds){
		        	
		        	//labelTimerSeconds.scale = 0.01;
		        	CCScaleTo rot11 = CCScaleTo.action( 0.03f ,1.2f); 
		            CCScaleTo rot21 = CCScaleTo.action( 0.07f , 1.0f); 
		            CCSequence seq1 = CCSequence.actions(rot11, rot21);
		            labelTimerSeconds.runAction(seq1);
		            labelTimerSeconds.setString(secondsString);
		        }
		        
		        oldMinutes = minutes;
		        oldSeconds = seconds;
	    	 }else if(GlobalSingleton.timeLeft==0 && GlobalSingleton.golAnimationPlaying==true){
	 			
	    	 }else if(GlobalSingleton.timeLeft==0 && GlobalSingleton.golAnimationPlaying==false){
	    		 
	    		unschedule(updateTimer);
	 			unschedule(startPlayerTurn);
	 			unschedule(startDelayRestart);
	 			unschedule(tickCallback);
	 			 
	 			if(GlobalSingleton.defaultTimeLeft==300){
	 				GlobalSingleton.leaderboardPlayTime = 1;
	 			}else if(GlobalSingleton.defaultTimeLeft==480){
	 				GlobalSingleton.leaderboardPlayTime = 2;
	 			}else if(GlobalSingleton.defaultTimeLeft==600){
	 				GlobalSingleton.leaderboardPlayTime = 3;
	 			}else{
	 				GlobalSingleton.leaderboardPlayTime = 1;
	 			}
	 			
	 			 if(GlobalSingleton.gameType == 0){
	 				
					//GAME TYPE 0  worldcup
					//GAME TYPE 1 versus
					//GAME TYPE 2 Multiplayer
					//GAME TYPE 3 single
					//GAME TYPE 5 penalty - not here check PenaltyBallLayer 

	 				//FASE 1
					GlobalSingleton.champGoalsConceded += GlobalSingleton.currentScoreP2;
	 				
	 				//Log.i(TAG,GlobalSingleton.currentScoreP1+":"+GlobalSingleton.currentScoreP2+":"+GlobalSingleton.atualMatch);
	 				if(GlobalSingleton.atualMatch<=2){
						
							//winner
							CCDirector.sharedDirector().replaceScene(AfterGameMatchsScene.scene());
	
						//FASE 2 - tournament
					}else if(GlobalSingleton.atualMatch > 2){
							if(GlobalSingleton.currentScoreP1 > GlobalSingleton.currentScoreP2){
								//winner
								CCDirector.sharedDirector().replaceScene(MatchWinnerScene.scene());
	
							}else if(GlobalSingleton.currentScoreP1 < GlobalSingleton.currentScoreP2){
								//loser	
								CCDirector.sharedDirector().replaceScene(MatchLoserScene.scene());
							}else{
								//draw
								CCDirector.sharedDirector().replaceScene(MatchDrawScene.scene());
							}
					}
				}else{
					//GAME TYPE 1 versus
					//GAME TYPE 2 Multiplayer
					//GAME TYPE 3 single
					//GAME TYPE 5 penalty - not here check PenaltyBallLayer 
					if(GlobalSingleton.currentScoreP1 > GlobalSingleton.currentScoreP2){
						//winner
						CCDirector.sharedDirector().replaceScene(MatchWinnerScene.scene());
						
					}else if(GlobalSingleton.currentScoreP1< GlobalSingleton.currentScoreP2){
						//loser	
						CCDirector.sharedDirector().replaceScene(MatchLoserScene.scene());
						
					}else{
						//Log.i(TAG,"MENU SCENE GO!");
						//DRAW
						CCDirector.sharedDirector().replaceScene(MatchDrawScene.scene());
					}	
	 				
	 			}
	    	}	
	    	
	  }

		
		
		private UpdateCallback startDelayRestart = new UpdateCallback() {
			
			@Override
			public void update(float d) {
				tickStartDelayRestart(d);
			}
		};
		public synchronized void tickStartDelayRestart(float delta) {
//			synchronized (_world) {
//	    		_world.step(delta, 8, 1);
//	    	}
	    	
			if( GlobalSingleton.timeToDelayAction==4){
	    		GlobalSingleton.gameTurn = 4;
	    		unschedule(startPlayerTurn);
	    	}
	    	
			if( GlobalSingleton.timeToDelayAction<=0){
	    		GlobalSingleton.maxExecutionTime = 0;
	    		removeChild(100,true);
	    		GlobalSingleton.timeToDelayAction = 4;
	    		GlobalSingleton.golAnimationPlaying = false;
	    		GlobalSingleton.gameTurn = 0;
	    		unschedule(startDelayRestart);
	    		
	    	}
	    	//Log.e("timeToDelayAction", "timeToDelayAction :"+GlobalSingleton.timeToDelayAction);
	    	GlobalSingleton.timeToDelayAction -= 1; 
	    }
		
		
		
	    private UpdateCallback startPlayerTurn = new UpdateCallback() {
			
			@Override
			public void update(float d) {
				tickStartPlayerTurn(d);
			}
		};
		public synchronized void tickStartPlayerTurn(float delta) {
			//Log.e("startPlayerTurn", "startPlayerTurn :"+GlobalSingleton.timePlayTurn+" gtype:"+GlobalSingleton.gameType);
	    	
//			synchronized (_world) {
//	    		_world.step(delta, 8, 1);
//	    	}
	     	//WORLD CUP
	    	if((GlobalSingleton.timePlayTurn==6 && GlobalSingleton.gameType==0 ) || 
	    			//MULTIPLAYER
	    			( GlobalSingleton.timePlayTurn == 12 && GlobalSingleton.gameType==1 ) ||
                    //SINGLE MATCH
                    ( GlobalSingleton.timePlayTurn==6 && GlobalSingleton.gameType==2  ) ||
                    //VERSUS
                   (GlobalSingleton.timePlayTurn==6 && GlobalSingleton.gameType==3 &&
                            GlobalSingleton.isP1==true))
	    	{
	    		
		    	if(GlobalSingleton.gameType == 1){
		    		GlobalSingleton.isP1 = true;
		    		GlobalSingleton.isP2 = true;
		    	}
		    	
		    	//GOL ANIMATION
		    	CCAnimation clockAnim = CCAnimation.animation("clock");
		    	clockAnim.addFrame("clock1.png");
		    	clockAnim.addFrame("clock2.png");
		    	clockAnim.addFrame("clock3.png");
		    	clockAnim.addFrame("clock4.png");
		    	clockAnim.addFrame("clock5.png");
		    	clockAnim.addFrame("clock6.png");
		    	clockAnim.addFrame("clock7.png");
		    	clockAnim.addFrame("clock8.png");
		    	clockAnim.addFrame("clock9.png");
		    	clockAnim.addFrame("clock10.png");
		    	clockAnim.addFrame("clock11.png");
		    	clockAnim.addFrame("clock12.png");
		    	clockAnim.addFrame("clock13.png");
		    	clockAnim.addFrame("clock14.png");
		    	clockAnim.addFrame("clock15.png");
		    	clockAnim.addFrame("clock16.png");
		    	clockAnim.addFrame("clock17.png");
		    	clockAnim.addFrame("clock18.png");
		    	clockAnim.addFrame("clock19.png");
		    	clockAnim.addFrame("clock20.png");
		    	clockAnim.addFrame("clock21.png");
		    	clockAnim.addFrame("clock22.png");
		    	clockAnim.addFrame("clock23.png");
		    	clockAnim.addFrame("clock24.png");
		    	
		    	CCAnimate clockAction = CCAnimate.action(4.0f, clockAnim, false);
		    	clockTurnSprite = new CCSprite("clock1.png");
		    	clockTurnSprite.setScale(scaleFactor);
		    	clockTurnSprite.setPosition( CGPoint.make(180*scaleFactor, 298*scaleFactor));
		    	this.addChild(clockTurnSprite,200,210);
		    	// Make a sprite (defined elsewhere) actually perform the animation
		    	clockTurnSprite.runAction(clockAction);
		    	//VERSUS
	    	}else if((GlobalSingleton.timePlayTurn==6 && GlobalSingleton.gameType==1) || 
	    			//MULTIPLAYER
	    			(GlobalSingleton.timePlayTurn==6 && GlobalSingleton.gameType==3 &&
	    					GlobalSingleton.isP2==true)){
	    		
	    			removeChild(clockTurnSprite, true);
	    			
	    			//Hide arrows for versus mode
	    			if(GlobalSingleton.gameType == 1){
	    				GlobalSingleton.isP1 = false;
	    				GlobalSingleton.isP2 = true;
	    				CCSprite arrowSelected;
	    				arrowSelected = (CCSprite)_p1.getChild(201);
	    				arrowSelected.setScaleX(0.1f);
	    				arrowSelected = (CCSprite)_p2.getChild(201);
	    				arrowSelected.setScaleX(0.1f);
	    				arrowSelected = (CCSprite)_p3.getChild(201);
	    				arrowSelected.setScaleX(0.1f);
	    			}
	    			
	    			//GOL ANIMATION
			    	CCAnimation clockAnim = CCAnimation.animation("clock");
			    	clockAnim.addFrame("clock1.png");
			    	clockAnim.addFrame("clock2.png");
			    	clockAnim.addFrame("clock3.png");
			    	clockAnim.addFrame("clock4.png");
			    	clockAnim.addFrame("clock5.png");
			    	clockAnim.addFrame("clock6.png");
			    	clockAnim.addFrame("clock7.png");
			    	clockAnim.addFrame("clock8.png");
			    	clockAnim.addFrame("clock9.png");
			    	clockAnim.addFrame("clock10.png");
			    	clockAnim.addFrame("clock11.png");
			    	clockAnim.addFrame("clock12.png");
			    	clockAnim.addFrame("clock13.png");
			    	clockAnim.addFrame("clock14.png");
			    	clockAnim.addFrame("clock15.png");
			    	clockAnim.addFrame("clock16.png");
			    	clockAnim.addFrame("clock17.png");
			    	clockAnim.addFrame("clock18.png");
			    	clockAnim.addFrame("clock19.png");
			    	clockAnim.addFrame("clock20.png");
			    	clockAnim.addFrame("clock21.png");
			    	clockAnim.addFrame("clock22.png");
			    	clockAnim.addFrame("clock23.png");
			    	clockAnim.addFrame("clock24.png");
			    	CCAnimate clockAction = CCAnimate.action(4.0f, clockAnim, false);
			    	clockTurnSprite = new CCSprite("clock1.png");
			    	clockTurnSprite.setPosition( CGPoint.make(302*scaleFactor, 298*scaleFactor));
			    	clockTurnSprite.setScale(scaleFactor);
			    	this.addChild(clockTurnSprite,200,210);
			    	// Make a sprite (defined elsewhere) actually perform the animation
			    	clockTurnSprite.runAction(clockAction);
	    	}
	    	if(GlobalSingleton.timePlayTurn>0){
	    		GlobalSingleton.timePlayTurn -= 1;
	    	}else{
	    		if(GlobalSingleton.gameType == 1){
	    			GlobalSingleton.isP1 = false;
	    			GlobalSingleton.isP2 = false;
	    		}
	    		removeChild(clockTurnSprite,true);
	    		CCSprite arrowSelected;
				arrowSelected = (CCSprite)_p1.getChild(201);
				arrowSelected.setScaleX(0.1f);
				arrowSelected = (CCSprite)_p2.getChild(201);
				arrowSelected.setScaleX(0.1f);
				arrowSelected = (CCSprite)_p3.getChild(201);
				arrowSelected.setScaleX(0.1f);
				arrowSelected = (CCSprite)_p4.getChild(201);
				arrowSelected.setScaleX(0.1f);
				arrowSelected = (CCSprite)_p5.getChild(201);
				arrowSelected.setScaleX(0.1f);
				arrowSelected = (CCSprite)_p6.getChild(201);
				arrowSelected.setScaleX(0.1f);
				if(GlobalSingleton.gameType == 3){
					GlobalSingleton.gameTurn = 5;
					if(hasSentPosition == false){
						hasSentPosition = true;
						GlobalSingleton.p1Position = CGPoint.make(p1Body.getPosition().x,p1Body.getPosition().y);
						GlobalSingleton.p2Position = CGPoint.make(p2Body.getPosition().x,p2Body.getPosition().y);
						GlobalSingleton.p3Position = CGPoint.make(p3Body.getPosition().x,p3Body.getPosition().y);
						GlobalSingleton.p4Position = CGPoint.make(p4Body.getPosition().x,p4Body.getPosition().y);
						GlobalSingleton.p5Position = CGPoint.make(p5Body.getPosition().x,p5Body.getPosition().y);
						GlobalSingleton.p6Position = CGPoint.make(p6Body.getPosition().x,p6Body.getPosition().y);
						GlobalSingleton.ballPosition = CGPoint.make(ballBody.getPosition().x,ballBody.getPosition().y);
						//SEND DATA MULTIPLAYER
						//	[(SoccerVirtualCupAppDelegate*)[[UIApplication sharedApplication] delegate] mySendData];
					}
					if((GlobalSingleton.hasSynced2==true && GlobalSingleton.isP1 == true && hasSentPosition == true) ||
							(GlobalSingleton.hasSynced1==true && GlobalSingleton.isP2 == true && hasSentPosition == true) 
							){
						hasSentPosition = false;
						GlobalSingleton.gameTurn = 2;
						GlobalSingleton.hasSynced1 = false;
						GlobalSingleton.hasSynced2 = false;
						schedule(tickCallback, 1/60);
						unschedule(startPlayerTurn);
					}
				}else{
					
					GlobalSingleton.gameTurn = 2;
					schedule(tickCallback, 1/60);
					unschedule(startPlayerTurn);
				}
	    	}
	    }
		
		
		private UpdateCallback tickCallback = new UpdateCallback() {
			
			@Override
			public void update(float d) {
				tick(d);
			}
		};
		
		Body p1Body,p2Body,p3Body,p4Body,p5Body,p6Body,ballBody;
		
		public synchronized void tick(float delta) {
		    	synchronized (_world) {
		    		_world.step(delta, 8, 2);
		    	}
		    	
				
		    	// Iterate over the bodies in the physics world
		    	Iterator<Body> it = _world.getBodies();
		    	while(it.hasNext()) {
		    		Body b = it.next();
		    		Object userData = b.getUserData();
		    		
		    		if (userData != null && userData instanceof CCSprite) {
		    			
		    			//Synchronize the Sprites position and rotation with the corresponding body
		    			CCSprite sprite = (CCSprite)userData;
		    			sprite.setPosition(b.getPosition().x * PTM_RATIO, b.getPosition().y * PTM_RATIO);
		    			if(sprite.getTag()==1001 || sprite.getTag()==1002 || sprite.getTag()==1003 ||
		    					sprite.getTag()==1004 || sprite.getTag()==1005 || sprite.getTag()==1006){
	//	    				(sprite.getChild(1)).setRotation(-1.0f * ccMacros.CC_RADIANS_TO_DEGREES(b.getAngle()));
		    				(sprite.getChild(1)).setRotation(-0.05f * ccMacros.CC_RADIANS_TO_DEGREES(b.getAngle()));
		    			}else{
		    				//sprite.setRotation(-1.0f * ccMacros.CC_RADIANS_TO_DEGREES(b.getAngle()));
		    			//	sprite.setRotation(-0.05f * ccMacros.CC_RADIANS_TO_DEGREES(b.getAngle()));
		    				
		    			}
		    			
		    		}
		    	}
		    	////////////////////////
		    	//
				//		MAIN LOOP
				//
				////////////////////////
		    	/*
		    	 * Log.e(TAG,"LOOP GAME TURN : " + GlobalSingleton.gameTurn + 
		    			"\n --gameType : " + GlobalSingleton.gameType +  
		    			"\n --ballPosition"+ballBody.getPosition().x);*/
		    	//reset match
		    	if(GlobalSingleton.gameTurn==0){
		    		this.whistleAudio();
					p1Body.setTransform(new Vector2(140*scaleFactor/PTM_RATIO, 240*scaleFactor/PTM_RATIO),0);
		    		p2Body.setTransform(new Vector2(100*scaleFactor/PTM_RATIO, 153*scaleFactor/PTM_RATIO),0);
		    		p3Body.setTransform(new Vector2(140*scaleFactor/PTM_RATIO, 66*scaleFactor/PTM_RATIO),0);
		    		p4Body.setTransform(new Vector2(340*scaleFactor/PTM_RATIO, 240*scaleFactor/PTM_RATIO),0);
		    		p5Body.setTransform(new Vector2(380*scaleFactor/PTM_RATIO, 153*scaleFactor/PTM_RATIO),0);
		    		p6Body.setTransform(new Vector2(340*scaleFactor/PTM_RATIO, 66*scaleFactor/PTM_RATIO),0);
		    		ballBody.setTransform(new Vector2(240*scaleFactor/PTM_RATIO, 153*scaleFactor/PTM_RATIO),0);
	
		    		//[self clearBodyImpulseAndVectors];
		    		GlobalSingleton.p1Position = CGPoint.make(140*scaleFactor/PTM_RATIO, 240*scaleFactor/PTM_RATIO);
		    		GlobalSingleton.p2Position = CGPoint.make(100*scaleFactor/PTM_RATIO, 153*scaleFactor/PTM_RATIO);
		    		GlobalSingleton.p3Position = CGPoint.make(140*scaleFactor/PTM_RATIO, 66*scaleFactor/PTM_RATIO);
		    		GlobalSingleton.p4Position = CGPoint.make(340*scaleFactor/PTM_RATIO, 240*scaleFactor/PTM_RATIO);
		    		GlobalSingleton.p5Position = CGPoint.make(380*scaleFactor/PTM_RATIO, 153*scaleFactor/PTM_RATIO);
		    		GlobalSingleton.p6Position = CGPoint.make(340*scaleFactor/PTM_RATIO, 66*scaleFactor/PTM_RATIO);
		    		GlobalSingleton.ballPosition = CGPoint.make(240*scaleFactor/PTM_RATIO, 153*scaleFactor/PTM_RATIO);
		    		GlobalSingleton.p1ForceVector = CGPoint.zero();
		    		GlobalSingleton.p2ForceVector = CGPoint.zero();
		    		GlobalSingleton.p3ForceVector = CGPoint.zero();
		    		GlobalSingleton.p4ForceVector = CGPoint.zero();
		    		GlobalSingleton.p5ForceVector = CGPoint.zero();
		    		GlobalSingleton.p6ForceVector = CGPoint.zero();
		    		_world.clearForces();
		    		GlobalSingleton.gameTurn = 1;
	
		    	//first turn
		    	}else if( GlobalSingleton.gameTurn == 1 ){
		    		
		    	
		    		if(itsYourTurn==null){
		    			
		    			itsYourTurn = new CCSprite("its_your_turn.png");
		    			itsYourTurn.setPosition(CGPoint.make(240*scaleFactor, 40*scaleFactor));
		    			itsYourTurn.setScale(scaleFactor);
		    			this.addChild(itsYourTurn ,310 ,310);
		    			new Timer().schedule(new TimerTask() {          
		    			    @Override
		    			    public void run() {
		    			        // this code will be executed after 5 seconds    
		    			    	removeChild(itsYourTurn, true);
		    			    	itsYourTurn = null;
		    			    }
		    			}, 5000);
		    		}
		    	
		    	
		    		if( GlobalSingleton.gameType == 1 ){
		    			GlobalSingleton.timePlayTurn = 12;
		    			unschedule(tickCallback);
		    			schedule(startPlayerTurn, 1);
                    }else if( GlobalSingleton.gameType == 2 ){
                        EnemyAI.aiPlayer(CGPoint.make(ballBody.getPosition().x,ballBody.getPosition().y),
                                CGPoint.make(p1Body.getPosition().x,p1Body.getPosition().y),
                                CGPoint.make(p2Body.getPosition().x,p2Body.getPosition().y),
                                CGPoint.make(p3Body.getPosition().x,p3Body.getPosition().y),
                                CGPoint.make(p4Body.getPosition().x,p4Body.getPosition().y),
                                CGPoint.make(p5Body.getPosition().x,p5Body.getPosition().y),
                                CGPoint.make(p6Body.getPosition().x,p6Body.getPosition().y));
                        GlobalSingleton.timePlayTurn = 6;
                        unschedule(tickCallback);
                        schedule(startPlayerTurn, 1);

                }else if( GlobalSingleton.gameType == 3 ){
						//SEND SYNC POSITION
		    			if(hasSentPosition==false){
		    				hasSentPosition = true;
		    				
		    				GlobalSingleton.p1Position = CGPoint.make(p1Body.getPosition().x, p1Body.getPosition().y);
		    				GlobalSingleton.p2Position = CGPoint.make(p2Body.getPosition().x, p2Body.getPosition().y);
		    				GlobalSingleton.p3Position = CGPoint.make(p3Body.getPosition().x, p3Body.getPosition().y);
		    				GlobalSingleton.p4Position = CGPoint.make(p4Body.getPosition().x, p4Body.getPosition().y);
		    				GlobalSingleton.p5Position = CGPoint.make(p5Body.getPosition().x, p5Body.getPosition().y);
		    				GlobalSingleton.p6Position = CGPoint.make(p6Body.getPosition().x, p6Body.getPosition().y);
		    				GlobalSingleton.ballPosition = CGPoint.make(ballBody.getPosition().x, ballBody.getPosition().y);
		    				//verificar como mandar dados no android
		    				//[(SoccerVirtualCupAppDelegate*)[[UIApplication sharedApplication] delegate] mySendDataSyncPosition];
		    			}
		    			if((GlobalSingleton.hasSynced2==true && GlobalSingleton.isP1==true && hasSentPosition==true)  || (GlobalSingleton.hasSynced1==true && GlobalSingleton.isP2==true && hasSentPosition==true)){
		    				p1Body.setTransform(new Vector2(GlobalSingleton.p1Position.x, GlobalSingleton.p1Position.y), p1Body.getAngle());
		    				p2Body.setTransform(new Vector2(GlobalSingleton.p2Position.x, GlobalSingleton.p2Position.y), p2Body.getAngle());
		    				p3Body.setTransform(new Vector2(GlobalSingleton.p3Position.x, GlobalSingleton.p3Position.y), p3Body.getAngle());
		    				p4Body.setTransform(new Vector2(GlobalSingleton.p4Position.x, GlobalSingleton.p4Position.y), p4Body.getAngle());
		    				p5Body.setTransform(new Vector2(GlobalSingleton.p5Position.x, GlobalSingleton.p5Position.y), p5Body.getAngle());
		    				p6Body.setTransform(new Vector2(GlobalSingleton.p6Position.x, GlobalSingleton.p6Position.y), p6Body.getAngle());
		    				ballBody.setTransform(new Vector2(GlobalSingleton.ballPosition.x, GlobalSingleton.ballPosition.y), ballBody.getAngle());
	    					
		    				GlobalSingleton.timePlayTurn = 6;
		    				hasSentPosition = false;
		    					GlobalSingleton.hasSynced1 = false;
		    					GlobalSingleton.hasSynced2 = false;
		    					unschedule(tickCallback);
		    					schedule(startPlayerTurn, 1);
		    				}
		    				
		    			}
		    	// set forces to apply and hide arrows
			}else if( GlobalSingleton.gameTurn == 2 ){
				//Log.e(TAG,"GAME TURN 2");
				//this.clearBodyImpulseAndVectors();
				
//				p1Body.setTransform(new Vector2(GlobalSingleton.p1Position.x,GlobalSingleton.p1Position.y),p1Body.getAngle());
//				p2Body.setTransform(new Vector2(GlobalSingleton.p2Position.x,GlobalSingleton.p2Position.y),p2Body.getAngle());
//				p3Body.setTransform(new Vector2(GlobalSingleton.p3Position.x,GlobalSingleton.p3Position.y),p3Body.getAngle());
//				p4Body.setTransform(new Vector2(GlobalSingleton.p4Position.x,GlobalSingleton.p4Position.y),p4Body.getAngle());
//				p5Body.setTransform(new Vector2(GlobalSingleton.p5Position.x,GlobalSingleton.p5Position.y),p5Body.getAngle());
//				p6Body.setTransform(new Vector2(GlobalSingleton.p6Position.x,GlobalSingleton.p6Position.y),p6Body.getAngle());
//				ballBody.setTransform(new Vector2(GlobalSingleton.ballPosition.x,GlobalSingleton.p1Position.y),ballBody.getAngle());	
				waitPlaying = new CCSprite("wait_playing.png");
				waitPlaying.setPosition(240*scaleFactor, 40*scaleFactor);
				waitPlaying.setScale(scaleFactor);
				this.addChild(waitPlaying ,310 ,310);
				new Timer().schedule(new TimerTask() {          
				    @Override
				    public void run() {
				        // this code will be executed after 5 seconds    
				    	removeChild(waitPlaying, true);
				    	
				    }
				}, 2000);
				
				Log.i(TAG,"p1ForceVector x:"+GlobalSingleton.p1ForceVector.x + "  y:"+GlobalSingleton.p1ForceVector.y); 
				Log.i(TAG,"p2ForceVector x:"+GlobalSingleton.p2ForceVector.x + "  y:"+GlobalSingleton.p2ForceVector.y); 
				Log.i(TAG,"p3ForceVector x:"+GlobalSingleton.p3ForceVector.x + "  y:"+GlobalSingleton.p3ForceVector.y); 
				Log.i(TAG,"p4ForceVector x:"+GlobalSingleton.p4ForceVector.x + "  y:"+GlobalSingleton.p4ForceVector.y); 
				Log.i(TAG,"p5ForceVector x:"+GlobalSingleton.p5ForceVector.x + "  y:"+GlobalSingleton.p5ForceVector.y); 
				Log.i(TAG,"p6ForceVector x:"+GlobalSingleton.p6ForceVector.x + "  y:"+GlobalSingleton.p6ForceVector.y); 
				
				if(GlobalSingleton.p1ForceVector.x != 0 && GlobalSingleton.p1ForceVector.y != 0 ) p1Body.applyLinearImpulse(new Vector2(GlobalSingleton.p1ForceVector.x,GlobalSingleton.p1ForceVector.y),new Vector2(0,0) );
				if(GlobalSingleton.p2ForceVector.x != 0 && GlobalSingleton.p2ForceVector.y != 0 ) p2Body.applyLinearImpulse(new Vector2(GlobalSingleton.p2ForceVector.x,GlobalSingleton.p2ForceVector.y),new Vector2(0,0) );
				if(GlobalSingleton.p3ForceVector.x != 0 && GlobalSingleton.p3ForceVector.y != 0 ) p3Body.applyLinearImpulse(new Vector2(GlobalSingleton.p3ForceVector.x,GlobalSingleton.p3ForceVector.y),new Vector2(0,0) );
				if(GlobalSingleton.p4ForceVector.x != 0 && GlobalSingleton.p4ForceVector.y != 0 ) p4Body.applyLinearImpulse(new Vector2(GlobalSingleton.p4ForceVector.x,GlobalSingleton.p4ForceVector.y),new Vector2(0,0) );
				if(GlobalSingleton.p5ForceVector.x != 0 && GlobalSingleton.p5ForceVector.y != 0 ) p5Body.applyLinearImpulse(new Vector2(GlobalSingleton.p5ForceVector.x,GlobalSingleton.p5ForceVector.y),new Vector2(0,0) );
				if(GlobalSingleton.p6ForceVector.x != 0 && GlobalSingleton.p6ForceVector.y != 0 ) p6Body.applyLinearImpulse(new Vector2(GlobalSingleton.p6ForceVector.x,GlobalSingleton.p6ForceVector.y),new Vector2(0,0) );
				
				GlobalSingleton.gameTurn=3;
			// start turn execution
			}else if(GlobalSingleton.gameTurn==3){
				if(GlobalSingleton.cornerImpulse !=0 ){
					if(GlobalSingleton.cornerImpulse == 1)ballBody.applyLinearImpulse(new Vector2(1000,1000),new Vector2(0,0));
					if(GlobalSingleton.cornerImpulse == 2)ballBody.applyLinearImpulse(new Vector2(1000,-700),new Vector2(0,0));
					if(GlobalSingleton.cornerImpulse == 3)ballBody.applyLinearImpulse(new Vector2(-500,1000),new Vector2(0,0));
					if(GlobalSingleton.cornerImpulse == 4)ballBody.applyLinearImpulse(new Vector2(-500,-700),new Vector2(0,0));
					GlobalSingleton.cornerImpulse = 0;
				}
//				if([self cornerImpulse]!=0){
//					if([self cornerImpulse]==1)cpBodyApplyImpulse(ballBody, cpv(1000,1000),cpv(0,0));  
//					if([self cornerImpulse]==2)cpBodyApplyImpulse(ballBody, cpv(1000,-680),cpv(0,0));  
//					if([self cornerImpulse]==3)cpBodyApplyImpulse(ballBody, cpv(-520,1000),cpv(0,0));  
//					if([self cornerImpulse]==4)cpBodyApplyImpulse(ballBody, cpv(-520,-680),cpv(0,0));  
//					[self setCornerImpulse:0];
//				}
//				if([self wallImpulse]!=0){
//					//hit top
//					if([self wallImpulse]==1)cpBodyApplyImpulse(ballBody, cpv(0,-1000),cpv(0,0));  
//					//hit rightt
//					if([self wallImpulse]==2)cpBodyApplyImpulse(ballBody, cpv(-1000,0),cpv(0,0));  
//					//hit bottom
//					if([self wallImpulse]==3)cpBodyApplyImpulse(ballBody, cpv(0,1000),cpv(0,0));  
//					//hit left
//					if([self wallImpulse]==4)cpBodyApplyImpulse(ballBody, cpv(1000,0),cpv(0,0));  
//					[self setWallImpulse:0];
//					
//				}
				
				// get ready
				if(GlobalSingleton.maxExecutionTime==160){
					getReady = new CCSprite("get_ready.png");
					getReady.setPosition(240*scaleFactor, 40*scaleFactor);
					getReady.setScale(scaleFactor);
					this.addChild(getReady,310 ,310);
					new Timer().schedule(new TimerTask() {          
	    			    @Override
	    			    public void run() {
	    			        // this code will be executed after 5 seconds    
	    			    	removeChild(getReady, true);
	    			    }
	    			},2000);
				}
				
				if((_ball.getPosition().x == GlobalSingleton.ballPosition.x && _ball.getPosition().y == GlobalSingleton.ballPosition.y &&
				   _p1.getPosition().x == GlobalSingleton.p1Position.x && _p1.getPosition().y == GlobalSingleton.p1Position.y &&
				   _p2.getPosition().x == GlobalSingleton.p2Position.x && _p2.getPosition().y == GlobalSingleton.p2Position.y &&
				   _p3.getPosition().x == GlobalSingleton.p3Position.x && _p3.getPosition().y == GlobalSingleton.p3Position.y &&
				   _p4.getPosition().x == GlobalSingleton.p4Position.x && _p4.getPosition().y == GlobalSingleton.p4Position.y &&
				   _p5.getPosition().x == GlobalSingleton.p5Position.x && _p5.getPosition().y == GlobalSingleton.p5Position.y &&
				   _p6.getPosition().x == GlobalSingleton.p6Position.x && _p6.getPosition().y == GlobalSingleton.p6Position.y  && GlobalSingleton.golAnimationPlaying ==false) 
				   || (GlobalSingleton.maxExecutionTime >=250 && GlobalSingleton.golAnimationPlaying == false )){
					
					GlobalSingleton.checkSleep = GlobalSingleton.checkSleep+1;
					
					if(GlobalSingleton.checkSleep==4){
						GlobalSingleton.maxExecutionTime =0;
						GlobalSingleton.checkSleep = 0;
						GlobalSingleton.p1Position = CGPoint.make(p1Body.getPosition().x,p1Body.getPosition().y);
						GlobalSingleton.p2Position = CGPoint.make(p2Body.getPosition().x,p2Body.getPosition().y);
						GlobalSingleton.p3Position = CGPoint.make(p3Body.getPosition().x,p3Body.getPosition().y);
						GlobalSingleton.p4Position = CGPoint.make(p4Body.getPosition().x,p4Body.getPosition().y);
						GlobalSingleton.p5Position = CGPoint.make(p5Body.getPosition().x,p5Body.getPosition().y);
						GlobalSingleton.p6Position= CGPoint.make(p6Body.getPosition().x,p6Body.getPosition().y);
						GlobalSingleton.ballPosition = CGPoint.make(ballBody.getPosition().x,ballBody.getPosition().y);
						GlobalSingleton.p1ForceVector = CGPoint.zero();
						GlobalSingleton.p2ForceVector = CGPoint.zero();
						GlobalSingleton.p3ForceVector = CGPoint.zero();
						GlobalSingleton.p4ForceVector = CGPoint.zero();
						GlobalSingleton.p5ForceVector = CGPoint.zero();
						GlobalSingleton.p6ForceVector = CGPoint.zero();
						GlobalSingleton.gameTurn = 1;
						_world.clearForces();
					}
					
				}
				GlobalSingleton.maxExecutionTime = GlobalSingleton.maxExecutionTime+1;

			}else if(GlobalSingleton.gameTurn==4){	
			}

	    	
		}
		
		@Override
	    public void ccAccelerometerChanged(float accelX, float accelY, float accelZ) {

		    // Landscape values
//	    	Vector2 gravity = new Vector2(accelY * 15, -accelX * 15);
	    	Vector2 gravity = new Vector2(-accelX * 15, -accelY * 15);
	    	_world.setGravity(gravity);

		}
		
		
		private float kSlide = 1.5f;
		
		@Override
		public boolean ccTouchesMoved(MotionEvent event){
			float kPowFactor = 70  * kSlide;
			//if(GlobalSingleton.gameTurn==1){}
			
			CGPoint location = CCDirector.sharedDirector().convertToGL(CGPoint.ccp(event.getX(), event.getY()));
			
			touchSprite.setPosition(location.x,location.y);
			CCSprite arrowSelected;
			if(GlobalSingleton.currentSelectedP1==1){
				float b = (location.y-_p1.getPosition().y);
				float c = (location.x-_p1.getPosition().x);
				double degreeAngle = Math.atan2(b,c)*180/ 3.14565656;
				double a = Math.sqrt(b*b+c*c);
				double scaleSize = Math.round(10/5.9*a)/100;
				arrowSelected = (CCSprite)_p1.getChild(201);
				arrowSelected.setRotation((float) -degreeAngle);
				if(scaleSize>1.2){
					scaleSize = 1.2;
				}
				arrowSelected.setScaleX((float) scaleSize);
				double d = scaleSize*Math.cos(-Math.atan2(b,c));
				double e = -scaleSize*Math.sin(-Math.atan2(b,c));
				if(scaleSize>0.25)GlobalSingleton.p1ForceVector = CGPoint.make((float)(d*kPowFactor),(float)(e*kPowFactor));
				
			}else if(GlobalSingleton.currentSelectedP1==2){
				float b = (location.y-_p2.getPosition().y);
				float c = (location.x-_p2.getPosition().x);
				double degreeAngle = Math.atan2(b,c)*180/ 3.14565656;
				double a = Math.sqrt(b*b+c*c);
				double scaleSize = Math.round(10/5.9*a)/100;
				arrowSelected = (CCSprite)_p2.getChild(201);
				arrowSelected.setRotation((float) -degreeAngle);
				if(scaleSize>1.2){
					scaleSize = 1.2;
				}
				arrowSelected.setScaleX((float) scaleSize);
				double d = scaleSize*Math.cos(-Math.atan2(b,c));
				double e = -scaleSize*Math.sin(-Math.atan2(b,c));
				if(scaleSize>0.25)GlobalSingleton.p2ForceVector = CGPoint.make((float)(d*kPowFactor),(float)(e*kPowFactor));
				
			}else if(GlobalSingleton.currentSelectedP1==3){
				float b = (location.y-_p3.getPosition().y);
				float c = (location.x-_p3.getPosition().x);
				double degreeAngle = Math.atan2(b,c)*180/ 3.14565656;
				double a = Math.sqrt(b*b+c*c);
				double scaleSize = Math.round(10/5.9*a)/100;
				arrowSelected = (CCSprite)_p3.getChild(201);
				arrowSelected.setRotation((float) -degreeAngle);
				if(scaleSize>1.2){
					scaleSize = 1.2;
				}
				arrowSelected.setScaleX((float) scaleSize);
				double d = scaleSize*Math.cos(-Math.atan2(b,c));
				double e = -scaleSize*Math.sin(-Math.atan2(b,c));
				if(scaleSize>0.25)GlobalSingleton.p3ForceVector = CGPoint.make((float)(d*kPowFactor),(float)(e*kPowFactor));
				
			}else if(GlobalSingleton.currentSelectedP2==4){
				float b = (location.y-_p4.getPosition().y);
				float c = (location.x-_p4.getPosition().x);
				double degreeAngle = Math.atan2(b,c)*180/ 3.14565656;
				double a = Math.sqrt(b*b+c*c);
				double scaleSize = Math.round(10/5.9*a)/100;
				arrowSelected = (CCSprite)_p4.getChild(201);
				arrowSelected.setRotation((float) -degreeAngle);
				if(scaleSize>1.2){
					scaleSize = 1.2;
				}
				arrowSelected.setScaleX((float) scaleSize);
				double d = scaleSize*Math.cos(-Math.atan2(b,c));
				double e = -scaleSize*Math.sin(-Math.atan2(b,c));
				if(scaleSize>0.25)GlobalSingleton.p4ForceVector = CGPoint.make((float)(d*kPowFactor),(float)(e*kPowFactor));
				
			}else if(GlobalSingleton.currentSelectedP2==5){
				float b = (location.y-_p5.getPosition().y);
				float c = (location.x-_p5.getPosition().x);
				double degreeAngle = Math.atan2(b,c)*180/ 3.14565656;
				double a = Math.sqrt(b*b+c*c);
				double scaleSize = Math.round(10/5.9*a)/100;
				arrowSelected = (CCSprite)_p5.getChild(201);
				arrowSelected.setRotation((float) -degreeAngle);
				if(scaleSize>1.2){
					scaleSize = 1.2;
				}
				arrowSelected.setScaleX((float) scaleSize);
				double d = scaleSize*Math.cos(-Math.atan2(b,c));
				double e = -scaleSize*Math.sin(-Math.atan2(b,c));
				if(scaleSize>0.25)GlobalSingleton.p5ForceVector = CGPoint.make((float)(d*kPowFactor),(float)(e*kPowFactor));
				
			}else if(GlobalSingleton.currentSelectedP2==6){
				float b = (location.y-_p6.getPosition().y);
				float c = (location.x-_p6.getPosition().x);
				double degreeAngle = Math.atan2(b,c)*180/ 3.14565656;
				double a = Math.sqrt(b*b+c*c);
				double scaleSize = Math.round(10/5.9*a)/100;
				arrowSelected = (CCSprite)_p6.getChild(201);
				arrowSelected.setRotation((float) -degreeAngle);
				if(scaleSize>1.2){
					scaleSize = 1.2;
				}
				arrowSelected.setScaleX((float) scaleSize);
				double d = scaleSize*Math.cos(-Math.atan2(b,c));
				double e = -scaleSize*Math.sin(-Math.atan2(b,c));
				if(scaleSize>0.25)GlobalSingleton.p6ForceVector = CGPoint.make((float)(d*kPowFactor),(float)(e*kPowFactor));
				
			}
			return false;
			
			
		}
		
		@Override
		public boolean ccTouchesBegan(MotionEvent event) {
			Log.e(TAG,"touchBegan");
			GlobalSingleton.currentSelectedP1 = 0;
			GlobalSingleton.currentSelectedP2 = 0;
			
			CGPoint location = CCDirector.sharedDirector().convertToGL(CGPoint.ccp(event.getX(), event.getY()));
					
			
			
			if (new Rect((int)(_p1.getPosition().x-_p1.getContentSize().width/2), 
					(int)(_p1.getPosition().y-_p1.getContentSize().height/2),
					(int)(_p1.getPosition().x+_p1.getContentSize().width/2),
					(int)(_p1.getPosition().y+_p1.getContentSize().height/2)).contains((int)location.x,(int)location.y) && GlobalSingleton.isP1==true) {
				GlobalSingleton.currentSelectedP1 = 1;
				Log.e("TESTE","player 1");
			}else if (new Rect((int)(_p2.getPosition().x-_p2.getContentSize().width/2), 
					(int)(_p2.getPosition().y-_p2.getContentSize().height/2),
					(int)(_p2.getPosition().x+_p2.getContentSize().width/2),
					(int)(_p2.getPosition().y+_p2.getContentSize().height/2)).contains((int)location.x,(int)location.y) && GlobalSingleton.isP1==true) {
				GlobalSingleton.currentSelectedP1 = 2;
				Log.e("TESTE","player 2");
			}else if (new Rect((int)(_p3.getPosition().x-_p3.getContentSize().width/2), 
					(int)(_p3.getPosition().y-_p3.getContentSize().height/2),
					(int)(_p3.getPosition().x+_p3.getContentSize().width/2),
					(int)(_p3.getPosition().y+_p3.getContentSize().height/2)).contains((int)location.x,(int)location.y) && GlobalSingleton.isP1==true) {
				GlobalSingleton.currentSelectedP1 = 3;
				Log.e("TESTE","player 3");
			}else if (new Rect((int)(_p4.getPosition().x-_p4.getContentSize().width/2), 
					(int)(_p4.getPosition().y-_p4.getContentSize().height/2),
					(int)(_p4.getPosition().x+_p4.getContentSize().width/2),
					(int)(_p4.getPosition().y+_p4.getContentSize().height/2)).contains((int)location.x,(int)location.y) && GlobalSingleton.isP2==true) {
				GlobalSingleton.currentSelectedP2 = 4;
			}else if (new Rect((int)(_p5.getPosition().x-_p5.getContentSize().width/2), 
					(int)(_p5.getPosition().y-_p5.getContentSize().height/2),
					(int)(_p5.getPosition().x+_p5.getContentSize().width/2),
					(int)(_p5.getPosition().y+_p5.getContentSize().height/2)).contains((int)location.x,(int)location.y) && GlobalSingleton.isP2==true) {
				GlobalSingleton.currentSelectedP2 = 5;
			}else if (new Rect((int)(_p6.getPosition().x-_p6.getContentSize().width/2), 
					(int)(_p6.getPosition().y-_p6.getContentSize().height/2),
					(int)(_p6.getPosition().x+_p6.getContentSize().width/2),
					(int)(_p6.getPosition().y+_p6.getContentSize().height/2)).contains((int)location.x,(int)location.y) && GlobalSingleton.isP2==true) {
				GlobalSingleton.currentSelectedP2 = 6;
			}
			
		    return true;
		}
		
		
		@Override
		public boolean ccTouchesEnded(MotionEvent event) {
			touchSprite.setPosition(10000,10000);
		    return super.ccTouchesEnded(event);
		}
	


		
		  //****************
		 // AUDIO
		 //****************
		 private void ballAudio(){
			 Context context = CCDirector.sharedDirector().getActivity();
			 SoundEngine.sharedEngine().preloadEffect(context, R.raw.kick);
			 SoundEngine.sharedEngine().preloadEffect(context, R.raw.kick1);
				
			 if(GlobalSingleton.audioOn==true){
		 		if(GlobalSingleton.ballAudio==false){
		 			GlobalSingleton.ballAudio = true;
		 			int r = (int) (Math.random() % 2);
		 			SoundEngine sae = new SoundEngine();
		 			
		 			if(r==1){
		 				sae.playEffect(context, R.raw.kick);
		 			}else{
		 				sae.playEffect(context, R.raw.kick1);
		 			}
		 			
/*		 			new Timer().schedule(new TimerTask() {          
	    			    private Handler updateUI = new Handler(){
	    			    	@Override
	    			    	public void dispatchMessage(Message msg) {
	    			    		super.dispatchMessage(msg);
			 					ballAudioReset();
	    			    	}
	    			    };
	    			    
		 				@Override
	    			    public void run() {
	    			        // this code will be executed after 5 seconds    
	    			    	updateUI.sendEmptyMessage(0);
	    			    }
	    			},600);
	*/		
		 		}
		 	}
		 }
		 private void gameCrowdAudio(){
			 Context context = CCDirector.sharedDirector().getActivity();
			 SoundEngine.sharedEngine().preloadEffect(context, R.raw.ambience);
			 SoundEngine.sharedEngine().preloadEffect(context, R.raw.ambience2);
		 	if(GlobalSingleton.audioOn==false){
		 		int r = (int) (Math.random() % 2);
			 	if(r==1){
			 		SoundEngine.sharedEngine().playEffect(context, R.raw.ambience);
			 	}else{
			 		SoundEngine.sharedEngine().playEffect(context, R.raw.ambience2);
			 		//[[SimpleAudioEngine sharedEngine] playEffect:@"ambience2.mp3" pitch:1 pan:0.5 gain:0.15];
			 	}
		 	}
		 }
		 private void crowdGoalAudio(){
			Context context = CCDirector.sharedDirector().getActivity();
			SoundEngine.sharedEngine().preloadEffect(context, R.raw.crowd);
		 	if(GlobalSingleton.audioOn==true){
		 		SoundEngine.sharedEngine().playEffect(context, R.raw.crowd);
		 	}
		 }

		 public void ballAudioReset(){
		 	GlobalSingleton.ballAudio=false;
		 }
		 private void whistleAudio(){
			 Context context = CCDirector.sharedDirector().getActivity();
			 SoundEngine.sharedEngine().preloadEffect(context, R.raw.whistle);
			 if(GlobalSingleton.audioOn==true){
		 		SoundEngine.sharedEngine().playEffect(context, R.raw.whistle);
		 	}
		 }

		
}

