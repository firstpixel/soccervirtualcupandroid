package com.firstpixel.soccervirtualcup;

public class Team {

	private int id;
	private String name;
	private String shortName;
	private String img1;
	private String img2;
	private int factor;
	
	public Team(int _id, String _name,String _shortName,String _img1, String _img2, int _factor) {
		// TODO Auto-generated constructor stub
		setId(_id);
		setName(_name);
		setShortName(_shortName);
		setImg1(_img1);
		setImg2(_img2);
		setFactor(_factor);
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getImg1() {
		return img1;
	}

	public void setImg1(String img1) {
		this.img1 = img1;
	}

	public String getImg2() {
		return img2;
	}

	public void setImg2(String img2) {
		this.img2 = img2;
	}

	public int getFactor() {
		return factor;
	}

	public void setFactor(int factor) {
		this.factor = factor;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}



}
