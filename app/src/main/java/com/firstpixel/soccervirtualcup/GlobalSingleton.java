package com.firstpixel.soccervirtualcup;

import java.util.Random;

import org.cocos2d.types.CGPoint;
import android.content.Context;
import android.content.SharedPreferences;


public class GlobalSingleton {

    private static GlobalSingleton singleton;
    public GlobalSingleton(){
    	
    	singleton = this;
    }
    public static GlobalSingleton sharedInstance(  )
    {
         return singleton;
    }
    public static float scaleFactor;
    public static int penaltyAttack;
    public static int maxExecutionTime;
    public static int gameType = 1;
    public static String teste;
    public static String teamName1;
    public static String teamName2;
    public static boolean musicOn;
    public static boolean sfxOn;
    public static int gameKitState;
    
    public static int playerType;
    public static int gameTurn = 0;
    public static int currentSelectedP1;
    public static int currentSelectedP2;
    public static boolean isP1;
    public static boolean isP2;
    public static boolean gamePlayed;
    public static CGPoint p1ForceVector;
    public static CGPoint p2ForceVector;
    public static CGPoint p3ForceVector;
    public static CGPoint p4ForceVector;
    public static CGPoint p5ForceVector;
    public static CGPoint p6ForceVector;
    
    public static int timeLeft;
	public static boolean ballAudio;
    public static boolean audioOn;
	public static boolean golAnimationPlaying;
	public static int defaultTimeLeft = 600;
	public static int leaderboardPlayTime;
    
	public static boolean goal1Scored = false;
	public static boolean goal2Scored = false;
    
	public static int currentScoreP1 = 0;
	public static int currentScoreP2 = 0;
    
	
	public static int timeToDelayAction;
	
	public static int myTeamID;
	public static int atualMatch;
	
	public static int matchWeather;
	
	public static int  teamsArray [];//32  // c-style array
	public static int  lastMatchScoreArray [];//32
	
	public static int lastTournamentScoreArray [];//32
	
	public static int pointsArray [];//32
	public static int winsArray [];//32
	public static int lossArray [];//32
	public static int drawArray [];//32
	public static int goalScoredArray [];//32
	public static int goalConcededArray [];//32
	public static int goalDifferenceArray [];//32
	
	public static int p1p2Array []; //8
	public static int p1p3Array []; //8
	public static int p1p4Array []; //8
	
	public static int p2p1Array []; //8
	public static int p2p3Array []; //8
	public static int p2p4Array []; //8
	
	public static int p3p1Array []; //8
	public static int p3p2Array []; //8
	public static int p3p4Array []; //8
	
	public static int p4p1Array []; //8
	public static int p4p2Array []; //8
	public static int p4p3Array []; //8
	
	public static int spritePlayer1;
	public static int spritePlayer2;
	
	public static int tournament1T1;
	public static int tournament1T2;
	public static int tournament1S1;
	public static int tournament1S2;
	public static int tournament2T1;
	public static int tournament2T2;
	public static int tournament2S1;
	public static int tournament2S2;
	public static int tournament3T1;
	public static int tournament3T2;
	public static int tournament3S1;
	public static int tournament3S2;
	public static int tournament4T1;
	public static int tournament4T2;
	public static int tournament4S1;
	public static int tournament4S2;
	public static int tournament5T1;
	public static int tournament5T2;
	public static int tournament5S1;
	public static int tournament5S2;
	public static int tournament6T1;
	public static int tournament6T2;
	public static int tournament6S1;
	public static int tournament6S2;
	public static int tournament7T1;
	public static int tournament7T2;
	public static int tournament7S1;
	public static int tournament7S2;
	public static int tournament8T1;
	public static int tournament8T2;
	public static int tournament8S1;
	public static int tournament8S2;
	public static int tournament9T1;
	public static int tournament9T2;
	public static int tournament9S1;
	public static int tournament9S2;
	public static int tournament10T1;
	public static int tournament10T2;
	public static int tournament10S1;
	public static int tournament10S2;
	public static int tournament11T1;
	public static int tournament11T2;
	public static int tournament11S1;
	public static int tournament11S2;
	public static int tournament12T1;
	public static int tournament12T2;
	public static int tournament12S1;
	public static int tournament12S2;
	public static int tournament13T1;
	public static int tournament13T2;
	public static int tournament13S1;
	public static int tournament13S2;
	public static int tournament14T1;
	public static int tournament14T2;
	public static int tournament14S1;
	public static int tournament14S2;
	public static int tournament15T1;
	public static int tournament15T2;
	public static int tournament15S1;
	public static int tournament15S2;
	public static int tournament16T1;
	public static int tournament16T2;
	public static int tournament16S1;
	public static int tournament16S2;
	
	public static int champGoalsConceded;
	public static int sevenInARow;
	

	public static SoccerVirtualCupActivity appContext;
	public static CGPoint p1Position;
	public static CGPoint p2Position;
	public static CGPoint p3Position;
	public static CGPoint p4Position;
	public static CGPoint p5Position;
	public static CGPoint p6Position;
	public static CGPoint ballPosition;
	public static int timePlayTurn;
	public static boolean hasSynced1;
	public static boolean hasSynced2;
	public static int checkSleep;
	
	public static CGPoint p1StartVector;
	public static CGPoint p2StartVector;
	public static CGPoint p3StartVector;
	public static CGPoint p4StartVector;
	public static CGPoint p5StartVector;
	public static CGPoint p6StartVector;
	public static CGPoint ballStartVector;
	public static int cornerImpulse;
	public static int wallImpulse;
	
	
	

	public static void clearData() {
		cornerImpulse = 0;
		wallImpulse = 0;
		
		ballAudio=false;
		checkSleep = 0;
		new CGPoint();
		p1ForceVector = CGPoint.zero();
		p2ForceVector = CGPoint.zero();
		p3ForceVector = CGPoint.zero();
		p4ForceVector = CGPoint.zero();
		p5ForceVector = CGPoint.zero();
		p6ForceVector = CGPoint.zero();
		
		p1StartVector = CGPoint.make(140, 240);
		p2StartVector = CGPoint.make(100, 153);
		p3StartVector = CGPoint.make(140, 66);
		p4StartVector = CGPoint.make(340, 240);
		p5StartVector = CGPoint.make(380, 153);
		p6StartVector = CGPoint.make(340,66);
		ballStartVector = CGPoint.make(240,153);
		
		p1Position = CGPoint.make(140, 240);
		p2Position = CGPoint.make(100, 153);
		p3Position = CGPoint.make(140, 66);
		p4Position = CGPoint.make(340, 240);
		p5Position = CGPoint.make(380, 153);
		p6Position = CGPoint.make(340,66);
		ballPosition = CGPoint.make(240,153);
		
		playerType = 0;
	
		if(timeLeft==0)timeLeft = defaultTimeLeft;
		
		penaltyAttack = 0;
		hasSynced1 = false;
		hasSynced2 = false;
		currentScoreP1 = 0;
		currentSelectedP1 = 0;
		currentScoreP2 = 0;
		currentSelectedP2 = 0;
		timeToDelayAction = 4;
		timePlayTurn = 20;
		maxExecutionTime = 0;
		golAnimationPlaying = false;
		gamePlayed = false;
		musicOn = true;
		sfxOn = true;
		//start
		gameTurn = 0;
		gameKitState = 0;
	    
	}
	
    public void saveGameData(){
    	//NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    	SharedPreferences prefs = appContext.getSharedPreferences("com.firstpixel.SVC", Context.MODE_PRIVATE);
    	prefs.edit().putString("greeting", "MyLastCupGame");
    	prefs.edit().putBoolean("saved", true);
    	prefs.edit().putInt("myTeamID", myTeamID);
	    
    	for(int i=0;i<31;i++){
    		prefs.edit().putInt("lastMatchScoreArray"+i,lastMatchScoreArray[i]);
    		prefs.edit().putInt("pointsArray"+i,pointsArray[i]);
    		prefs.edit().putInt("winsArray"+i,winsArray[i]);
    		prefs.edit().putInt("lossArray"+i,lossArray[i]);
    		prefs.edit().putInt("drawArray"+i,drawArray[i]);
    		prefs.edit().putInt("goalScoredArray"+i,goalScoredArray[i]);
    		prefs.edit().putInt("goalConcededArray"+i,goalConcededArray[i]);
    		prefs.edit().putInt("goalDifferenceArray"+i,goalDifferenceArray[i]);
    	}
		
		for(int i=0;i<8;i++){
			prefs.edit().putInt("p1p2Array"+i,p1p2Array[i]);
			prefs.edit().putInt("p1p3Array"+i,p1p3Array[i]);
			prefs.edit().putInt("p1p4Array"+i,p1p4Array[i]);
			
			prefs.edit().putInt("p2p1Array"+i,p2p1Array[i]);
			prefs.edit().putInt("p2p3Array"+i,p2p3Array[i]);
			prefs.edit().putInt("p2p4Array"+i,p2p4Array[i]);
			
			prefs.edit().putInt("p3p1Array"+i,p3p1Array[i]);
			prefs.edit().putInt("p3p2Array"+i,p3p2Array[i]);
			prefs.edit().putInt("p3p4Array"+i,p3p4Array[i]);
			
			prefs.edit().putInt("p4p1Array"+i,p4p1Array[i]);
			prefs.edit().putInt("p4p2Array"+i,p4p2Array[i]);
			prefs.edit().putInt("p4p3Array"+i,p4p3Array[i]);

		}
	
		prefs.edit().putInt("spritePlayer1",spritePlayer1);
		prefs.edit().putInt("spritePlayer2",spritePlayer2);
		
		prefs.edit().putInt("currentScoreP1",currentScoreP1);
		prefs.edit().putInt("currentScoreP2",currentScoreP2);
		
		prefs.edit().putInt("matchWeather",matchWeather);
		prefs.edit().putInt("defaultTimeLeft",defaultTimeLeft);
		
		prefs.edit().putInt("atualMatch",atualMatch);
		
		prefs.edit().putInt("sevenInARow",sevenInARow);
		prefs.edit().putInt("champGoalsConceded",champGoalsConceded);
		
		prefs.edit().putInt("tournament1T1",tournament1T1);
		prefs.edit().putInt("tournament1T2",tournament1T2);
		prefs.edit().putInt("tournament1S1",tournament1S1);
		prefs.edit().putInt("tournament1S2",tournament1S2);

		prefs.edit().putInt("tournament2T1",tournament2T1);
		prefs.edit().putInt("tournament2T2",tournament2T2);
		prefs.edit().putInt("tournament2S1",tournament2S1);
		prefs.edit().putInt("tournament2S2",tournament2S2);
		
		prefs.edit().putInt("tournament3T1",tournament3T1);
		prefs.edit().putInt("tournament3T2",tournament3T2);
		prefs.edit().putInt("tournament3S1",tournament3S1);
		prefs.edit().putInt("tournament3S2",tournament3S2);

		prefs.edit().putInt("tournament4T1",tournament4T1);
		prefs.edit().putInt("tournament4T2",tournament4T2);
		prefs.edit().putInt("tournament4S1",tournament4S1);
		prefs.edit().putInt("tournament4S2",tournament4S2);
		
		prefs.edit().putInt("tournament5T1",tournament5T1);
		prefs.edit().putInt("tournament5T2",tournament5T2);
		prefs.edit().putInt("tournament5S1",tournament5S1);
		prefs.edit().putInt("tournament5S2",tournament5S2);
		
		prefs.edit().putInt("tournament6T1",tournament6T1);
		prefs.edit().putInt("tournament6T2",tournament6T2);
		prefs.edit().putInt("tournament6S1",tournament6S1);
		prefs.edit().putInt("tournament6S2",tournament6S2);
		
		prefs.edit().putInt("tournament7T1",tournament7T1);
		prefs.edit().putInt("tournament7T2",tournament7T2);
		prefs.edit().putInt("tournament7S1",tournament7S1);
		prefs.edit().putInt("tournament7S2",tournament7S2);
		
		prefs.edit().putInt("tournament8T1",tournament8T1);
		prefs.edit().putInt("tournament8T2",tournament8T2);
		prefs.edit().putInt("tournament8S1",tournament8S1);
		prefs.edit().putInt("tournament8S2",tournament8S2);
		
		prefs.edit().putInt("tournament9T1",tournament9T1);
		prefs.edit().putInt("tournament9T2",tournament9T2);
		prefs.edit().putInt("tournament9S1",tournament9S1);
		prefs.edit().putInt("tournament9S2",tournament9S2);
		
		prefs.edit().putInt("tournament10T1",tournament10T1);
		prefs.edit().putInt("tournament10T2",tournament10T2);
		prefs.edit().putInt("tournament10S1",tournament10S1);
		prefs.edit().putInt("tournament10S2",tournament10S2);
		
		prefs.edit().putInt("tournament11T1",tournament11T1);
		prefs.edit().putInt("tournament11T2",tournament11T2);
		prefs.edit().putInt("tournament11S1",tournament11S1);
		prefs.edit().putInt("tournament11S2",tournament11S2);
		
		prefs.edit().putInt("tournament12T1",tournament12T1);
		prefs.edit().putInt("tournament12T2",tournament12T2);
		prefs.edit().putInt("tournament12S1",tournament12S1);
		prefs.edit().putInt("tournament12S2",tournament12S2);
		
		prefs.edit().putInt("tournament13T1",tournament13T1);
		prefs.edit().putInt("tournament13T2",tournament13T2);
		prefs.edit().putInt("tournament13S1",tournament13S1);
		prefs.edit().putInt("tournament13S2",tournament13S2);
		
		prefs.edit().putInt("tournament14T1",tournament14T1);
		prefs.edit().putInt("tournament14T2",tournament14T2);
		prefs.edit().putInt("tournament14S1",tournament14S1);
		prefs.edit().putInt("tournament14S2",tournament14S2);
		
		prefs.edit().putInt("tournament15T1",tournament15T1);
		prefs.edit().putInt("tournament15T2",tournament15T2);
		prefs.edit().putInt("tournament15S1",tournament15S1);
		prefs.edit().putInt("tournament15S2",tournament15S2);
		
		prefs.edit().putInt("tournament16T1",tournament16T1);
		prefs.edit().putInt("tournament16T2",tournament16T2);
		prefs.edit().putInt("tournament16S1",tournament16S1);
		prefs.edit().putInt("tournament16S2",tournament16S2);
		
		prefs.edit().commit();
		
    }
    
	public static void loadGameData(){
		
		SharedPreferences prefs = appContext.getSharedPreferences("com.firstpixel.SVC", Context.MODE_PRIVATE);
		boolean saved = prefs.getBoolean("saved",false);
		if(saved){
			for(int i=0;i<31;i++){
				
				lastMatchScoreArray[i]= prefs.getInt("lastMatchScoreArray"+i,0);
				pointsArray[i] = prefs.getInt("pointsArray"+i,0);
				winsArray[i] = prefs.getInt("winsArray"+i,0);
				lossArray[i] = prefs.getInt("lossArray"+i,0);
				drawArray[i] = prefs.getInt("drawArray"+i,0);
				goalScoredArray[i] = prefs.getInt("goalScoredArray"+i,0);
				goalConcededArray[i] = prefs.getInt("goalConcededArray"+i,0);
				goalDifferenceArray[i] = prefs.getInt("goalDifferenceArray"+i,0);
			}
			
			for(int i=0;i<8;i++){
				p1p2Array[i] = prefs.getInt("p1p2Array"+i,0);
				p1p3Array[i] = prefs.getInt("p1p3Array"+i,0);
				p1p4Array[i] = prefs.getInt("p1p4Array"+i,0);
				p2p1Array[i] = prefs.getInt("p2p1Array"+i,0);
				p2p3Array[i] = prefs.getInt("p2p3Array"+i,0);
				p2p4Array[i] = prefs.getInt("p2p4Array"+i,0);
		
				p3p1Array[i] = prefs.getInt("p3p1Array"+i,0);
				p3p2Array[i] = prefs.getInt("p3p2Array"+i,0);
				p3p4Array[i] = prefs.getInt("p3p4Array"+i,0);
				p4p1Array[i] = prefs.getInt("p4p1Array"+i,0);
				p4p2Array[i] = prefs.getInt("p4p2Array"+i,0);
				p4p3Array[i] = prefs.getInt("p4p3Array"+i,0);
			}
			
			spritePlayer1 = prefs.getInt("spritePlayer1",0);
			spritePlayer2 = prefs.getInt("spritePlayer2",0);
			
			currentScoreP1 = prefs.getInt("currentScoreP1",0);
			currentScoreP2 = prefs.getInt("currentScoreP2",0);

			matchWeather = prefs.getInt("matchWeather",0);
			defaultTimeLeft = prefs.getInt("defaultTimeLeft",0);
			
			atualMatch = prefs.getInt("atualMatch",0);
			sevenInARow = prefs.getInt("sevenInARow",0);
			champGoalsConceded = prefs.getInt("champGoalsConceded",0);

			
			tournament1T1 = prefs.getInt("tournament1T1",100);
			tournament1T2 = prefs.getInt("tournament1T2",100);
			tournament1S1 = prefs.getInt("tournament1S1",100);
			tournament1S2 = prefs.getInt("tournament1S2",100);
		
			tournament2T1 = prefs.getInt("tournament2T1",100);
			tournament2T2 = prefs.getInt("tournament2T2",100);
			tournament2S1 = prefs.getInt("tournament2S1",100);
			tournament2S2 = prefs.getInt("tournament2S2",100);
		
			tournament3T1 = prefs.getInt("tournament3T1",100);
			tournament3T2 = prefs.getInt("tournament3T2",100);
			tournament3S1 = prefs.getInt("tournament3S1",100);
			tournament3S2 = prefs.getInt("tournament3S2",100);
		
			tournament4T1 = prefs.getInt("tournament4T1",100);
			tournament4T2 = prefs.getInt("tournament4T2",100);
			tournament4S1 = prefs.getInt("tournament4S1",100);
			tournament4S2 = prefs.getInt("tournament4S2",100);
		
			tournament5T1 = prefs.getInt("tournament5T1",100);
			tournament5T2 = prefs.getInt("tournament5T2",100);
			tournament5S1 = prefs.getInt("tournament5S1",100);
			tournament5S2 = prefs.getInt("tournament5S2",100);
		
			tournament6T1 = prefs.getInt("tournament6T1",100);
			tournament6T2 = prefs.getInt("tournament6T2",100);
			tournament6S1 = prefs.getInt("tournament6S1",100);
			tournament6S2 = prefs.getInt("tournament6S2",100);
		
			tournament7T1 = prefs.getInt("tournament7T1",100);
			tournament7T2 = prefs.getInt("tournament7T2",100);
			tournament7S1 = prefs.getInt("tournament7S1",100);
			tournament7S2 = prefs.getInt("tournament7S2",100);
		
			tournament8T1 = prefs.getInt("tournament8T1",100);
			tournament8T2 = prefs.getInt("tournament8T2",100);
			tournament8S1 = prefs.getInt("tournament8S1",100);
			tournament8S2 = prefs.getInt("tournament8S2",100);
		
			tournament9T1 = prefs.getInt("tournament9T1",100);
			tournament9T2 = prefs.getInt("tournament9T2",100);
			tournament9S1 = prefs.getInt("tournament9S1",100);
			tournament9S2 = prefs.getInt("tournament9S2",100);
		
			tournament10T1 = prefs.getInt("tournament10T1",100);
			tournament10T2 = prefs.getInt("tournament10T2",100);
			tournament10S1 = prefs.getInt("tournament10S1",100);
			tournament10S2 = prefs.getInt("tournament10S2",100);
		
			tournament11T1 = prefs.getInt("tournament11T1",100);
			tournament11T2 = prefs.getInt("tournament11T2",100);
			tournament11S1 = prefs.getInt("tournament11S1",100);
			tournament11S2 = prefs.getInt("tournament11S2",100);
		
			tournament12T1 = prefs.getInt("tournament12T1",100);
			tournament12T2 = prefs.getInt("tournament12T2",100);
			tournament12S1 = prefs.getInt("tournament12S1",100);
			tournament12S2 = prefs.getInt("tournament12S2",100);
		
			tournament13T1 = prefs.getInt("tournament13T1",100);
			tournament13T2 = prefs.getInt("tournament13T2",100);
			tournament13S1 = prefs.getInt("tournament13S1",100);
			tournament13S2 = prefs.getInt("tournament13S2",100);
		
			tournament14T1 = prefs.getInt("tournament14T1",100);
			tournament14T2 = prefs.getInt("tournament14T2",100);
			tournament14S1 = prefs.getInt("tournament14S1",100);
			tournament14S2 = prefs.getInt("tournament14S2",100);
		
			tournament15T1 = prefs.getInt("tournament15T1",100);
			tournament15T2 = prefs.getInt("tournament15T2",100);
			tournament15S1 = prefs.getInt("tournament15S1",100);
			tournament15S2 = prefs.getInt("tournament15S2",100);
		
			tournament16T1 = prefs.getInt("tournament16T1",100);
			tournament16T2 = prefs.getInt("tournament16T2",100);
			tournament16S1 = prefs.getInt("tournament16S1",100);
			tournament16S2 = prefs.getInt("tournament16S2",100);
			
		
		}else{
			clearArraysData();
		}
			
	}
	
	public static void saveTokenID(int tokenID){
		SharedPreferences prefs = appContext.getSharedPreferences("com.firstpixel.SVC", Context.MODE_PRIVATE);
		if(tokenID!=0){
			 int tokenID_ = tokenID;
	        prefs.edit().putInt("tokenID",tokenID_);
	        prefs.edit().commit();
	    }else{
	    
	    }  
		
	}
	public static void clearArraysData(){
		for(int i=0;i<31;i++){
			lastMatchScoreArray[i] = 0;
			pointsArray[i] = 0;
			winsArray[i] = 0;
			lossArray[i] = 0;
			drawArray[i] = 0;
			goalScoredArray[i] = 0;
			goalConcededArray[i] = 0;
			goalDifferenceArray[i] = 0;
		}
		for(int i=0;i<8;i++){
			p1p2Array[i] = 0;
			p1p3Array[i] = 0;
			p1p4Array[i] = 0;
			
			p2p1Array[i] = 0;
			p2p3Array[i] = 0;
			p2p4Array[i] = 0;

			p3p1Array[i] = 0;
			p3p2Array[i] = 0;
			p3p4Array[i] = 0;

			p4p1Array[i] = 0;
			p4p2Array[i] = 0;
			p4p3Array[i] = 0;
		}
		atualMatch = 0;
		sevenInARow = 0;
		champGoalsConceded = 0;
		
		tournament1T1 = 100;
		tournament1T2 = 100;
		tournament1S1 = 100;
		tournament1S2 = 100;
		tournament2T1 = 100;
		tournament2T2 = 100;
		tournament2S1 = 100;
		tournament2S2 = 100;
		tournament3T1 = 100;
		tournament3T2 = 100;
		tournament3S1 = 100;
		tournament3S2 = 100;
		tournament4T1 = 100;
		tournament4T2 = 100;
		tournament4S1 = 100;
		tournament4S2 = 100;
		tournament5T1 = 100;
		tournament5T2 = 100;
		tournament5S1 = 100;
		tournament5S2 = 100;
		tournament6T1 = 100;
		tournament6T2 = 100;
		tournament6S1 = 100;
		tournament6S2 = 100;
		tournament7T1 = 100;
		tournament7T2 = 100;
		tournament7S1 = 100;
		tournament7S2 = 100;
		tournament8T1 = 100;
		tournament8T2 = 100;
		tournament8S1 = 100;
		tournament8S2 = 100;
		tournament9T1 = 100;
		tournament9T2 = 100;
		tournament9S1 = 100;
		tournament9S2 = 100;
		tournament10T1 = 100;
		tournament10T2 = 100;
		tournament10S1 = 100;
		tournament10S2 = 100;
		tournament11T1 = 100;
		tournament11T2 = 100;
		tournament11S1 = 100;
		tournament11S2 = 100;
		tournament12T1 = 100;
		tournament12T2 = 100;
		tournament12S1 = 100;
		tournament12S2 = 100;
		tournament13T1 = 100;
		tournament13T2 = 100;
		tournament13S1 = 100;
		tournament13S2 = 100;
		tournament14T1 = 100;
		tournament14T2 = 100;
		tournament14S1 = 100;
		tournament14S2 = 100;
		tournament15T1 = 100;
		tournament15T2 = 100;
		tournament15S1 = 100;
		tournament15S2 = 100;
		tournament16T1 = 100;
		tournament16T2 = 100;
		tournament16S1 = 100;
		tournament16S2 = 100;

	}
	
	// return TokenID
	public static int returnTokenID(){
		SharedPreferences prefs = appContext.getSharedPreferences("com.firstpixel.SVC", Context.MODE_PRIVATE);
	    int tokenID = prefs.getInt("tokenID",-1);
		return tokenID;
	}
	// check if it has a TokenID
	public static boolean hasTokenID(){
		SharedPreferences prefs = appContext.getSharedPreferences("com.firstpixel.SVC", Context.MODE_PRIVATE);
	    if(prefs.getInt("tokenID", -1)!=-1){
	    	return true;
	    }else{
	    	return false;
	    }
	}

	public static int getP1P2Array (int group){
		return p1p2Array[group];
	}
	public static void setP1P2Array (int group, int value){
		p1p2Array[group] = value;
	}
	public static int getP1P3Array (int group){
		return p1p3Array[group];
	}
	public static void setP1P3Array (int group, int value){
		p1p3Array[group] = value;
	}
	public static int getP1P4Array (int group){
		return p1p4Array[group];
	}
	public static void setP1P4Array (int group, int value){
		p1p4Array[group] = value;
	}


	public static int getP2P1Array (int group){
		return p2p1Array[group];
	}
	public static void setP2P1Array (int group, int value){
		p2p1Array[group] = value;
	}
	public static int getP2P3Array (int group){
		return p2p3Array[group];
	}
	public static void setP2P3Array (int group, int value){
		p2p3Array[group] = value;
	}
	public static int getP2P4Array (int group){
		return p2p4Array[group];
	}
	public static void setP2P4Array (int group, int value){
		p2p4Array[group] = value;
	}

	
	public static int getP3P1Array (int group){
		return p3p1Array[group];
	}
	public static void setP3P1Array (int group, int value){
		p3p1Array[group] = value;
	}
	public static int getP3P2Array (int group){
		return p3p2Array[group];
	}
	public static void setP3P2Array (int group, int value){
		p3p2Array[group] = value;
	}
	public static int getP3P4Array (int group){
		return p3p4Array[group];
	}
	public static void setP3P4Array (int group, int value){
		p3p4Array[group] = value;
	}

	public static int getP4P1Array (int group){
		return p4p1Array[group];
	}
	public static void setP4P1Array (int group, int value){
		p4p1Array[group] = value;
	}
	public static int getP4P2Array (int group){
		return p4p2Array[group];
	}
	public static void setP4P2Array (int group, int value){
		p4p2Array[group] = value;
	}
	public static int getP4P3Array (int group){
		return p4p3Array[group];
	}
	public static void setP4P3Array (int group, int value){
		p4p3Array[group] = value;
	}

	public static int getLastMatchScoreByTeamKey (int x){
		return lastMatchScoreArray[x];
	}
	public static void setLastMatchScoreByTeamKey (int x, int value){
		lastMatchScoreArray[x] = value;
	}

	public static int getPointsArrayAtPos (int x){
		return pointsArray[x];
	}
	public static void setPointsArrayAtPos (int x, int value){
		pointsArray[x] = value;
	}

	public static int getWinsArrayAtPos (int x){
		return winsArray[x];
	}
	public static void setWinsArrayAtPos (int x, int value){
		winsArray[x] = value;
	}

	public static int getLossArrayAtPos (int x){
		return lossArray[x];
	}
	public static void setLossArrayAtPos (int x, int value){
		lossArray[x] = value;
	}

	public static int getDrawArrayAtPos (int x){
		return drawArray[x];
	}
	public static void setDrawArrayAtPos (int x, int value){
		drawArray[x] = value;
	}

	public static int getGoalScoredArrayAtPos (int x){
		return goalScoredArray[x];
	}
	public static void setGoalScoredArrayAtPos (int x, int value){
		goalScoredArray[x] = value;
	}
	
	public static int getGoalConcededArrayAtPos (int x){
		return goalConcededArray[x];
	}
	public static void setGoalConcededArrayAtPos (int x, int value){
		goalConcededArray[x] = value;
	}

	public static int getGoalDifferenceArrayAtPos (int x){
		return goalDifferenceArray[x];
	}
	public static void setGoalDifferenceArrayAtPos (int x, int value){
		goalDifferenceArray[x] = value;
	}

	public static int setMatchWinner(int p1, int p2, int goalp1, int goalp2 ){
		int winner = p1;
		int temp1;
		int temp2;
		if(goalp1 > goalp2){
			//p1 WIN
			//points
			temp1 = getPointsArrayAtPos(p1);
			setPointsArrayAtPos(p1,temp1+3);
			setWinsArrayAtPos(p1,getWinsArrayAtPos(p1)+1);
			setLossArrayAtPos(p2,getLossArrayAtPos(p2)+1);
		}else if( goalp1 < goalp2){
			//p2 WIN
			winner = p2;
			temp2 = getPointsArrayAtPos(p2);
			setPointsArrayAtPos(p2,temp2+3);
			setWinsArrayAtPos(p2,getWinsArrayAtPos(p2)+1);
			setLossArrayAtPos(p1,getLossArrayAtPos(p1)+1);
		
		}else{
			winner = -1;
			temp1 = getPointsArrayAtPos(p1);
			setPointsArrayAtPos(p1,temp1+1);
			temp2 = getPointsArrayAtPos(p2);
			setPointsArrayAtPos(p2,temp2+1);
			setDrawArrayAtPos(p1,getDrawArrayAtPos(p1)+1);
			setDrawArrayAtPos(p2,getDrawArrayAtPos(p2)+1);

		}
		
		setGoalScoredArrayAtPos(p1,getGoalScoredArrayAtPos(p1)+goalp1);
		setGoalConcededArrayAtPos(p1,getGoalConcededArrayAtPos(p1)+goalp2);
		setGoalDifferenceArrayAtPos(p1,getGoalDifferenceArrayAtPos(p1)+goalp1-goalp2);
		setGoalScoredArrayAtPos(p2,getGoalScoredArrayAtPos(p2)+goalp2);
		setGoalConcededArrayAtPos(p2,getGoalConcededArrayAtPos(p2)+goalp1);
		setGoalDifferenceArrayAtPos(p2,getGoalDifferenceArrayAtPos(p2)+goalp2-goalp1);
		
		setLastMatchScoreByTeamKey(p1,goalp1);
		setLastMatchScoreByTeamKey(p2,goalp2);

		return winner;
	}
	/************************
	 * Must create a DataHolder CLASS to hold data 
	 * from soccer teams to make it easy to change teams
	 * 
	 * 	getFactorByTeamKey must be implemented on this class
	 **********************/
	public static int getFactorByTeamKey(int p1){
		int factor =1;
		return factor;
	}
	public static int setRandomMatchWinner(int p1, int p2){
		
		int factor1 = getFactorByTeamKey(p1);
		int factor2 = getFactorByTeamKey(p2);
		
		Random rand = new Random();
		int r = rand.nextInt();
		int winner1 = r % factor1;
		r = rand.nextInt();
		int winner2 = r % factor2;
		
		//find winner based on best team
		int winner = p1;
		int goalp1;
		int goalp2;
		int temp1;
		int temp2;
		if(winner1 > winner2){
			//p1 WINNER
			winner = p1;
			r = rand.nextInt();
			goalp1 = (r % 5) + 1;
			goalp2 = goalp1 - (r % (goalp1-1));
			
			//points
			temp1 = getPointsArrayAtPos(p1);
			setPointsArrayAtPos(p1, temp1+3);
			setWinsArrayAtPos(p1, getWinsArrayAtPos(p1)+1);
			setLossArrayAtPos(p2, getLossArrayAtPos(p2)+1);
			
		} else if(winner2 > winner1){
			//p2 WINNER
			winner = p2;
			r = rand.nextInt();
			goalp2 = (r % 5) + 1;
			goalp1 = goalp2 - (r % (goalp2-1));
			
			//points
			temp2 = getPointsArrayAtPos(p2);
			setPointsArrayAtPos(p2, temp2+3);
			setWinsArrayAtPos(p2, getWinsArrayAtPos(p2)+1);
			setLossArrayAtPos(p1, getLossArrayAtPos(p1)+1);
			
		} else {
			//DRAW
			winner = -1;
			r = rand.nextInt();
			goalp1 = (r % 4) + 1;
			goalp2 = goalp1;
			
			//points
			temp1 = getPointsArrayAtPos(p1);
			setPointsArrayAtPos(p1, temp1+1);
			temp2 = getPointsArrayAtPos(p2);
			setPointsArrayAtPos(p2, temp2+1);
			setDrawArrayAtPos(p1, getDrawArrayAtPos(p1)+1);
			setDrawArrayAtPos(p2, getDrawArrayAtPos(p2)+1);
		}
		setGoalScoredArrayAtPos(p1, getGoalScoredArrayAtPos(p1)+goalp1);
		setGoalConcededArrayAtPos(p1, getGoalConcededArrayAtPos(p1)+goalp1);
		setGoalDifferenceArrayAtPos(p1, getGoalDifferenceArrayAtPos(p1)+goalp1-goalp2);

		setGoalScoredArrayAtPos(p2, getGoalScoredArrayAtPos(p2)+goalp2);
		setGoalConcededArrayAtPos(p2, getGoalConcededArrayAtPos(p2)+goalp2);
		setGoalDifferenceArrayAtPos(p2, getGoalDifferenceArrayAtPos(p2)+goalp2-goalp1);

		setLastMatchScoreByTeamKey(p1, goalp1);
		setLastMatchScoreByTeamKey(p2, goalp2);
		
		return winner;	
	}
	
	//TOURNAMENT
	public static int getLastTournamentScoreByTeamKey(int x){
		return lastMatchScoreArray[x];
	}

	public static void setLastTournamentScoreByTeamKey(int x, int value)
	{
		lastMatchScoreArray[x] = value;
	}
	
	public static int setTournamentWinner(int p1, int p2, int goalp1, int goalp2){
		int winner = p1;
		if(goalp1 > goalp2){
			winner = p1;
		} else {
			winner = p2;
		}
		setLastTournamentScoreByTeamKey(p1,goalp1);
		setLastTournamentScoreByTeamKey(p2,goalp2);
		return winner;
	}
	public static int setRandomTournamentWinner(int p1, int p2){

		int factor1 = getFactorByTeamKey(p1);
		int factor2 = getFactorByTeamKey(p2);
		
		Random rand = new Random();
		int r = rand.nextInt();
		int winner1 = r % factor1;
		r = rand.nextInt();
		int winner2 = r % factor2;
		
		//find winner based on best team
		int winner = p1;
		int goalp1;
		int goalp2;
		if(winner1 > winner2){
			//p1 WINNER
			winner = p1;
			r = rand.nextInt();
			goalp1 = (r % 5) + 1;
			goalp2 = goalp1 - (r % (goalp1-1));
		} else if(winner2 > winner1){
			//p2 WINNER
			winner = p2;
			r = rand.nextInt();
			goalp2 = (r % 5) + 1;
			goalp1 = goalp2 - (r % (goalp2-1));
		} else {
			//DRAW
			winner = p1;
			r = rand.nextInt();
			goalp1 = (r % 6) + 1;
			goalp2 = goalp1 - 1>=0?goalp1-1:0;
		}
		setLastTournamentScoreByTeamKey(p1,goalp1);
		setLastTournamentScoreByTeamKey(p2,goalp2);
		return winner;			
	}
	
	
}