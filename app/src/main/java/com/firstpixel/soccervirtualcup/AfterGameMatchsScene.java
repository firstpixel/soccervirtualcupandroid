package com.firstpixel.soccervirtualcup;
import org.cocos2d.layers.CCColorLayer;
import org.cocos2d.layers.CCScene;
import org.cocos2d.menus.CCMenu;
import org.cocos2d.menus.CCMenuItem;
import org.cocos2d.menus.CCMenuItemLabel;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCLabel;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.types.CGSize;
import org.cocos2d.types.ccColor3B;
import org.cocos2d.types.ccColor4B;

import android.view.MotionEvent;

public class AfterGameMatchsScene  extends CCColorLayer
	{
		private static final String TAG = "Soccer-AfterGameMatchsScene";
		protected CCLabel _label;
		protected CCMenuItem _menuItem1;
		protected CCMenu _menu;
		protected float scaleFactor = 1;
		
		public static CCScene scene()
		{
			CCScene scene = CCScene.node();
			CCColorLayer layer = new AfterGameMatchsScene(ccColor4B.ccc4(255, 255, 255, 255));
			scene.addChild(layer);
			
			return scene;
		}
		private void addBackGroundScene() {
	        CCSprite bgForLevel1 = CCSprite.sprite("screen/matchesresults_screen.jpg");
	        bgForLevel1 .setAnchorPoint(0, 0);	 
	        // Get window size
	    	CGSize s = CCDirector.sharedDirector().winSize();
	    	// Get scale
	    	float scaleX = s.width/bgForLevel1.getContentSize().width;
	        float scaleY = s.height/bgForLevel1.getContentSize().height;
	        // Set for minor scale
	        if(scaleX<scaleY){
	        	scaleFactor = scaleY;
	        	bgForLevel1.setPosition(s.width-(bgForLevel1.getContentSize().width*scaleFactor) ,0.0f);
	    	}else{
	        	scaleFactor = scaleX;        
	        	bgForLevel1.setPosition(0.0f,s.height-(bgForLevel1.getContentSize().height*scaleFactor) );
	    	}
	        bgForLevel1.setScale(scaleFactor);
			addChild(bgForLevel1 );
	    }
		
		protected AfterGameMatchsScene(ccColor4B color)
		{
			super(color);
			addBackGroundScene();
			CCLabel _labelPlay = CCLabel.makeLabel("Play Next Match", "DroidSans", 32);
			_labelPlay.setColor(ccColor3B.ccBLACK);
			_menuItem1 = CCMenuItemLabel.item(_labelPlay, this, "playGame");

			_menu = CCMenu.menu(_menuItem1);
			_menu.alignItemsVertically(150);
		    addChild(_menu);

		    this.setIsTouchEnabled(true);
		    
		}

		@Override
		public boolean ccTouchesBegan(MotionEvent event) {
		    return true;
		}
		
		
		@Override
		public boolean ccTouchesEnded(MotionEvent event) {
		    return super.ccTouchesEnded(event);
		}


		public void playGame(Object sender)
		{
			GlobalSingleton.teste = "play";
			CCDirector.sharedDirector().replaceScene(BallLayer.scene());
		}
		
	}
