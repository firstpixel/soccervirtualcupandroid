package com.firstpixel.soccervirtualcup;

import org.cocos2d.types.CGPoint;


public class EnemyAI {
	private static EnemyAI singleton;
	private static float kSlide = 1.5f;
	private static float scaleFactor = 70f;
	
	public EnemyAI(){
		singleton = this;
	}
	public static EnemyAI sharedInstance(  )
	{
	     return singleton;
	}
	public static void aiPlayer(CGPoint ballp, CGPoint p1p, CGPoint p2p, CGPoint p3p, CGPoint p4p, CGPoint p5p, CGPoint p6p)
	{
		scaleFactor = GlobalSingleton.scaleFactor;
		CGPoint golProPos = CGPoint.make(60*scaleFactor,153*scaleFactor);
		CGPoint golConPos = CGPoint.make(420*scaleFactor,153*scaleFactor);
		CGPoint backByLeftFromOppField = CGPoint.make(240*scaleFactor,0);
		CGPoint backByLeftFromOwenField = CGPoint.make(480*scaleFactor,0);
		CGPoint backByRightFromOppField = CGPoint.make(240*scaleFactor,320*scaleFactor);
		CGPoint backByRightFromOwenField = CGPoint.make(480*scaleFactor,320*scaleFactor);
		
		//STATE MACHINE
		boolean p4Attack = p4p.x > ballp.x?true:false;	
		boolean p5Attack = p5p.x > ballp.x?true:false;
		boolean p6Attack = p6p.x > ballp.x?true:false;
		
		CGPoint ballpHitAttack;
		//CGPoint ballpHitDefense;
		ballpHitAttack = CGPoint.make(ballp.x,ballp.y+(ballp.y-153*scaleFactor)/5);
		//ballpHitDefense = CGPoint.make(ballp.x+20,ballp.y+(ballp.y-153)/5);
		//TARGET 
		int hitRelevanceP1=0;
		int hitRelevanceP2=0;
		int hitRelevanceP3=0;

		double p1BallDistance = pointDistance(ballp,p1p);
		double p2BallDistance = pointDistance(ballp,p2p);
		double p3BallDistance = pointDistance(ballp,p3p);
		double p1ConGolDistance = pointDistance(golConPos,p1p);
		double p2ConGolDistance = pointDistance(golConPos,p2p);
		double p3ConGolDistance = pointDistance(golConPos,p3p);
	
		double p1ProGolDistance = pointDistance(golProPos,p1p);
		double p2ProGolDistance = pointDistance(golProPos,p2p);
		double p3ProGolDistance = pointDistance(golProPos,p3p);
		if( p1BallDistance < p2BallDistance && p1BallDistance < p3BallDistance ){
			if( p1p.x < ballp.x ){
				hitRelevanceP1=hitRelevanceP1+4;
			}else{
				hitRelevanceP1=hitRelevanceP1+2;
			}	
		}
	    
		if( p2BallDistance < p1BallDistance && p2BallDistance < p3BallDistance ){
			if(p2p.x<ballp.x){
				hitRelevanceP2=hitRelevanceP2+4;
			}else{
				hitRelevanceP2=hitRelevanceP2+2;
			}
		}
	    
		if(p3BallDistance<p1BallDistance && p3BallDistance<p2BallDistance){
			if(p3p.x<ballp.x){
				hitRelevanceP3=hitRelevanceP3+4;
			}else{
				hitRelevanceP3=hitRelevanceP3+2;
			}
		}
		
		if(p1ProGolDistance<p2ProGolDistance && p1ProGolDistance<p3ProGolDistance && ballp.x<=240*scaleFactor)hitRelevanceP1++;
		if(p2ProGolDistance<p1ProGolDistance && p2ProGolDistance<p3ProGolDistance && ballp.x<=240*scaleFactor)hitRelevanceP2++;
		if(p3ProGolDistance<p1ProGolDistance && p3ProGolDistance<p2ProGolDistance && ballp.x<=240*scaleFactor)hitRelevanceP3++;
	
		if(p1ConGolDistance<p2ConGolDistance && p1ConGolDistance<p3ConGolDistance  && ballp.x>240*scaleFactor){
			hitRelevanceP1++;
			if(p1p.x<ballp.x)hitRelevanceP1++;
		}
	    
		if(p2ConGolDistance<p1ConGolDistance && p2ConGolDistance<p3ConGolDistance  && ballp.x>240*scaleFactor){
			hitRelevanceP2++;
			if(p2p.x<ballp.x)hitRelevanceP2++;
		}
	    
		if(p3ConGolDistance<p1ConGolDistance && p3ConGolDistance<p2ConGolDistance  && ballp.x>240*scaleFactor){
			hitRelevanceP3++;
			if(p3p.x<ballp.x)hitRelevanceP3++;
		}
	    
		CGPoint oppHitTarget = null; 
		if(hitRelevanceP1<=hitRelevanceP2 && hitRelevanceP1<=hitRelevanceP3)oppHitTarget = p1p;
		if(hitRelevanceP2<=hitRelevanceP1 && hitRelevanceP2<=hitRelevanceP3)oppHitTarget = p2p;
		if(hitRelevanceP3<=hitRelevanceP1 && hitRelevanceP3<=hitRelevanceP2)oppHitTarget = p3p;
	
		//ATTACK
		int attackRelevanceP4=0;
		int attackRelevanceP5=0;
		int attackRelevanceP6=0;
		
		double p4BallDistance = pointDistance(ballp,p4p);
		double p5BallDistance = pointDistance(ballp,p5p);
		double p6BallDistance = pointDistance(ballp,p6p);
		
		double p4ProGolDistance =  pointDistance(golProPos,p4p);
		double p5ProGolDistance =  pointDistance(golProPos,p5p);
		double p6ProGolDistance =  pointDistance(golProPos,p6p);
		
		double p4ConGolDistance =  pointDistance(golConPos,p4p);
		double p5ConGolDistance =  pointDistance(golConPos,p5p);
		double p6ConGolDistance =  pointDistance(golConPos,p6p);
		
		if(p4BallDistance < p5BallDistance && p4BallDistance < p6BallDistance ){
			if(p4p.x>ballp.x){
				attackRelevanceP4=attackRelevanceP4+4;
			}else{
				attackRelevanceP4=attackRelevanceP4+2;
			}	
		}
	    
		if(p5BallDistance < p4BallDistance && p5BallDistance < p6BallDistance ){
			if(p5p.x<ballp.x){
				attackRelevanceP5=attackRelevanceP5+4;
			}else{
				attackRelevanceP5=attackRelevanceP5+2;
			}
		}
	    
		if(p6BallDistance < p4BallDistance && p6BallDistance < p5BallDistance ){
			if(p6p.x<ballp.x){
				attackRelevanceP6=attackRelevanceP6+4;
			}else{
				attackRelevanceP6=attackRelevanceP6+2;
			}
		}
	    
		if(p4ProGolDistance<p5ProGolDistance && p4ProGolDistance<p6ProGolDistance && p4p.x>ballp.x)attackRelevanceP4++;
		if(p5ProGolDistance<p4ProGolDistance && p5ProGolDistance<p6ProGolDistance && p5p.x>ballp.x)attackRelevanceP5++;
		if(p6ProGolDistance<p4ProGolDistance && p6ProGolDistance<p5ProGolDistance && p6p.x>ballp.x)attackRelevanceP6++;
		
		if(p4ConGolDistance<p5ConGolDistance && p4ConGolDistance<p6ConGolDistance && p4p.x>ballp.x)attackRelevanceP4++;
		if(p5ConGolDistance<p4ConGolDistance && p5ConGolDistance<p6ConGolDistance && p4p.x>ballp.x)attackRelevanceP5++;
		if(p6ConGolDistance<p4ConGolDistance && p6ConGolDistance<p5ConGolDistance && p4p.x>ballp.x)attackRelevanceP6++;
		
		//all defending set closest to strike ball
	 	if(p4Attack == false && p5Attack == false && p6Attack == false ){
			if(p4BallDistance<p5BallDistance && p4BallDistance<p6BallDistance)attackRelevanceP4++;
			if(p5BallDistance<p4BallDistance && p5BallDistance<p6BallDistance)attackRelevanceP5++;
			if(p6BallDistance<p4BallDistance && p6BallDistance<p5BallDistance)attackRelevanceP6++;
		}
		
		CGPoint aiP4ForceVector;
		CGPoint aiP5ForceVector;
		CGPoint aiP6ForceVector;
		
		if(p4Attack==true){
			// attack 
			if(attackRelevanceP4>attackRelevanceP5 && attackRelevanceP4>attackRelevanceP6){
				aiP4ForceVector = pointDistanceVector(CGPoint.make(ballpHitAttack.x+1,ballpHitAttack.y+1),p4p,6);
				
			}else{
				int r = (int) Math.round(Math.random() * 4);
				if(r==0){
					aiP4ForceVector = pointDistanceVector(oppHitTarget,p4p,4);
				}else{
					aiP4ForceVector = pointDistanceVector(ballpHitAttack,p4p,4);
				}
			}
		}else{
			//on the gol line, back by the sides
			if(p4p.y>70*scaleFactor && p4p.y<227*scaleFactor){
				if(p4p.y<217*scaleFactor){
					int r = (int) Math.round(Math.random() * 4);
					if(r==0){
						aiP4ForceVector = pointDistanceVector(oppHitTarget,p4p,5);
					}else{
						aiP4ForceVector = p4p.x<240*scaleFactor?pointDistanceVector(backByLeftFromOppField,p4p,5):pointDistanceVector(backByLeftFromOwenField,p4p,3);
					}	
				}else{
					aiP4ForceVector = p4p.x>=240*scaleFactor?pointDistanceVector(backByRightFromOppField,p4p,5):pointDistanceVector(backByRightFromOwenField,p4p,3);
				}
			}else{
				aiP4ForceVector = pointDistanceVector(golConPos,p4p,3);
			}
		}
		if(p5Attack==true){
			// attack 
			if(attackRelevanceP5>attackRelevanceP4 && attackRelevanceP5>attackRelevanceP6){
				aiP5ForceVector = pointDistanceVector(CGPoint.make(ballpHitAttack.x+1,ballpHitAttack.y+1), p5p ,6);
			}else{
				int r = (int) Math.round(Math.random() * 4);
				if(r==0){
					aiP5ForceVector = pointDistanceVector(oppHitTarget, p5p, 4);
				}else{
					aiP5ForceVector = pointDistanceVector(ballpHitAttack, p5p, 4);
					
				}
			}
		}else{
			//on the gol line, back by the sides
			if(p5p.y>70*scaleFactor && p5p.y<227*scaleFactor){
				if(p5p.y<217*scaleFactor){
					aiP5ForceVector = p5p.x < 240*scaleFactor ? pointDistanceVector(backByLeftFromOppField, p5p, 3):pointDistanceVector(backByLeftFromOwenField ,p5p ,3);
				}else{
					aiP5ForceVector = p5p.x >= 240*scaleFactor ? pointDistanceVector(backByRightFromOppField, p5p ,3):pointDistanceVector(backByRightFromOwenField ,p5p ,3);
				}
			}else{
				aiP5ForceVector = pointDistanceVector(golConPos ,p5p ,3);
			}
		}
		if(p6Attack==true){
			// attack 
			if(attackRelevanceP6>attackRelevanceP4 && attackRelevanceP6>attackRelevanceP5){
				aiP6ForceVector = pointDistanceVector(CGPoint.make(ballpHitAttack.x+1,ballpHitAttack.y+1) ,p6p ,6);
			}else{
				int r = (int) Math.round(Math.random() * 4);
				if(r==0){
					aiP6ForceVector = pointDistanceVector(oppHitTarget ,p6p ,4);
				}else{
					aiP6ForceVector = pointDistanceVector(ballpHitAttack ,p6p ,4);
				}
			}	
		}else{
			//on the gol line, back by the sides
			if(p6p.y>70*scaleFactor && p6p.y<227*scaleFactor){
				if(p6p.y<217*scaleFactor){
					aiP6ForceVector = p6p.x<240*scaleFactor?pointDistanceVector(backByLeftFromOppField,p6p ,3): pointDistanceVector(backByLeftFromOwenField,p6p ,3);
				}else{
					aiP6ForceVector = p6p.x>=240*scaleFactor?pointDistanceVector(backByRightFromOppField ,p6p ,3):pointDistanceVector(backByRightFromOwenField ,p6p,3);
				}
			}else{
				aiP6ForceVector = pointDistanceVector(golConPos, p6p, 3);
			}
		}
		float p4x = aiP4ForceVector.x>70?70:aiP4ForceVector.x<-70?-70:aiP4ForceVector.x;
		float p4y = aiP4ForceVector.y>70?70:aiP4ForceVector.y<-70?-70:aiP4ForceVector.y;
		
		float p5x = aiP5ForceVector.x>70?70:aiP5ForceVector.x<-70?-70:aiP5ForceVector.x;
		float p5y = aiP5ForceVector.y>70?70:aiP5ForceVector.y<-70?-70:aiP5ForceVector.y;
		
		float p6x = aiP6ForceVector.x>70?70:aiP6ForceVector.x<-70?-70:aiP6ForceVector.x;
		float p6y = aiP6ForceVector.y>70?70:aiP6ForceVector.y<-70?-70:aiP6ForceVector.y;
		
		GlobalSingleton.p4ForceVector = CGPoint.make(p4x,p4y);
		GlobalSingleton.p5ForceVector = CGPoint.make(p5x,p5y);
		GlobalSingleton.p6ForceVector = CGPoint.make(p6x,p6y);		
	}

	private static double pointDistance(CGPoint ballp, CGPoint p1p){
    	double b = (ballp.y-p1p.y);
    	double c = (ballp.x-p1p.x);
    	double a = Math.sqrt(b*b+c*c);
    	return a;
    }

    private static CGPoint pointDistanceVector(CGPoint p1 , CGPoint p2, int forceFactor){
    	int b = (int)(p1.y-p2.y);
    	int c = (int)(p1.x-p2.x);
    	return CGPoint.make((c*forceFactor*kSlide),(b*forceFactor*kSlide));
    }
}
